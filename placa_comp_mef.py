#!/usr/bin/env python
# -*- coding: utf-8
import numpy as np
from scipy import linalg
import math
import time

def fun_main():
    global nome_job
    global Entrada
    global HoraInicio
    global HoraAux
    global Desl
    global Arquivo_Entrada
    global Arquivo_Saida

    nome_job = raw_input("Digite o nome do job: ")
    
    # Abrir o arquivo de entrada
    Arquivo_Entrada = open(nome_job + ".txt")

    # Abrir o arquivo de saída
    Arquivo_Saida = open(nome_job +"_saida.txt",'w') 

    # Contabilizar o tempo
    HoraInicio = time.time()
    HoraAux = HoraInicio

    # Chamar a função dados
    fun_dados()
    fun_contar_tempo(time.time(),"dados")
    
    # Cálculos iniciais. Coordenadas, Incidencia, Propriedades
    fun_calculos_iniciais()
    fun_contar_tempo(time.time(),"cálculos iniciais")


    # Cálculos matriz constitutiva, Q, A, B e D
    fun_matriz_constitutiva()
    fun_contar_tempo(time.time(),"matriz constitutiva")


    # Matriz de rigidez local do elemento
    fun_rigidez_local()
    fun_contar_tempo(time.time(),"rigidez local")


    # Montar a matriz de rigidez global
    fun_rigidez_global()
    fun_contar_tempo(time.time(),"rigidez global")


    # Montar o vetor de cargas
    fun_vetor_carga()
    fun_contar_tempo(time.time(),"vetor de carga")


    # Condições de contorno
    fun_condicoes_contorno()
    fun_contar_tempo(time.time(),"condições de contorno")

    # Resolução do sistema
    #fun_resolucao_sistema()
    #Matriz = np.transpose(Matriz) # O solver do numpy recebe a matriz armazenada horizontalmente
    #Arquivo_Saida.write("Matriz transposta")
    #Arquivo_Saida.write()
    #print Banda
    #print Matriz
    #print len(Matriz),len(Matriz[0])
    #Desl = np.linalg.solve(Matriz, Vetor_Carga)
        
    Desl = linalg.solve_banded((Banda-1,Banda-1),Matriz,Vetor_Carga)
    fun_contar_tempo(time.time(),"resolução do sistema")

    # Calcular as derivadas
    fun_calcular_derivadas()
    fun_contar_tempo(time.time(),"derivadas")

    # Calcular os esforços internos
    fun_calcular_esforcos()
    fun_contar_tempo(time.time(),"esforços")


    # Calcular deformações e tensões
    fun_calcular_deformacoes_tensoes()
    fun_contar_tempo(time.time(),"deformações e tensões")

    # Mostrar resultados
    fun_escrever_resultados()
    fun_contar_tempo(time.time(),"escrever resultados")

    fun_contar_tempo(time.time(),"total")    
    

def fun_dados():
    global Num_Nos_Monitorados
    global Nos_Monitorados
    global La    
    global Lb
    global NEx
    global NEy
    global Num_Materiais
    global Materiais
    global Num_Laminas
    global Laminas
    global Carga_Distribuida
    global Carga_Distribuida_V
    global Carga_Trapezoidal
    global Carga_Trapezoidal_V
    global Cargas_Bordo
    global Cargas_Linha_X
    global Cargas_Linha_X_V
    global Cargas_Linha_Y
    global Cargas_Linha_Y_V
    global Cargas_Concentradas
    global Cargas_Concentradas_V
    global Restricoes_Bordo
    global Restricoes_Canto
    global Restricoes_Nos
    global Restricoes_Nos_V
    

    # Ler os dados no arquivo
    Entrada_prev = Arquivo_Entrada.read()
    Entrada_prev = Entrada_prev.split('\n')
    Arquivo_Entrada.close()
    
    # Retirar os "\r" que vem do arquivo de entrada de dados
    for i in range(len(Entrada_prev)): Entrada_prev[i] = Entrada_prev[i].replace("\r","")
    
    # Retirar as linhas comentadas, iniciando em #, e linhas em branco
    Entrada = []
    for I in range(len(Entrada_prev)):
        if not Entrada_prev[I].startswith("#") and not Entrada_prev[I].replace(" ","") == "":
            Entrada.append(Entrada_prev[I])
    
    linha = 0
    
    Num_Nos_Monitorados = int(Entrada[linha])
    linha += 1

    Nos_Monitorados = []
    for I in range(Num_Nos_Monitorados):
        Nos_Monitorados.append(int(Entrada[linha]))
        linha += 1
    Nos_Monitorados.sort() # colocar os nós em ordem crescente
    #print "Nós monitorados: ",Nos_Monitorados
    
    La = float(Entrada[linha])
    linha += 1
    Lb = float(Entrada[linha])
    linha += 1
    #print "Dimensões da placa: ", La, Lb
    
    NEx = int(Entrada[linha])
    linha += 1
    NEy = int(Entrada[linha])
    linha += 1
    #print "Subdivisões da placa: ", NEx, NEy    

    Num_Materiais = int(Entrada[linha])
    linha += 1
    #print "Número de materiais: ", Num_Materiais
    
    Materiais = []
    MateriaisL = []
    for I in range(Num_Materiais):
        MateriaisL = Entrada[linha].split(" ")
        MateriaisL[0] = int(MateriaisL[0])
        MateriaisL[1] = MateriaisL[1]
        MateriaisL[2] = float(MateriaisL[2])
        MateriaisL[3] = float(MateriaisL[3])
        MateriaisL[4] = float(MateriaisL[4])
        MateriaisL[5] = float(MateriaisL[5])
        Materiais.append(MateriaisL)
        linha += 1
    #print "Materiais: ", Materiais
    
    Num_Laminas = int(Entrada[linha])
    linha += 1
    #print "Número de Lâminas: ", Num_Laminas
    
    Laminas = []
    LaminasL = []
    for I in range(Num_Laminas):
        LaminasL = Entrada[linha].split(" ")
        LaminasL[0] = int(LaminasL[0])
        LaminasL[1] = float(LaminasL[1])
        LaminasL[2] = float(LaminasL[2])
        Laminas.append(LaminasL)
        linha += 1
    #print "Laminas: ", Laminas
    
    Carga_Distribuida = int(Entrada[linha])
    linha += 1
    if Carga_Distribuida == 1:
        Carga_Distribuida_V = float(Entrada[linha])
        linha += 1
        #print "Há carga distribuída."
        #print "q = ", Carga_Distribuida_V
    else:
        Carga_Distribuida_V = 0.0
        #print "Não há carga distribuída."
    
    Carga_Trapezoidal = int(Entrada[linha])
    linha += 1
    if Carga_Trapezoidal == 1:
        Carga_Trapezoidal_V = Entrada[linha]
        Carga_Trapezoidal_V = Carga_Trapezoidal_V.split(" ")
        Carga_Trapezoidal_V[0] = float(Carga_Trapezoidal_V[0])
        Carga_Trapezoidal_V[1] = float(Carga_Trapezoidal_V[1])
        linha += 1
        #print "Há carga trapezoidal."
        #print "q0,q1 = ", Carga_Trapezoidal_V
    #else:
        #print "Não há carga trapezoidal."
    
    # Cargas de bordo
    Cargas_Bordo = []
    Cargas_BordoL = []
    for I in range(4):
        Cargas_BordoL=(Entrada[linha])
        Cargas_BordoL = Cargas_BordoL.split(" ")
        for J in range(6): Cargas_BordoL[J] = float(Cargas_BordoL[J])
        Cargas_Bordo.append(Cargas_BordoL)
        linha += 1
    #print "Cargas de bordo: ", Cargas_Bordo
    
    Cargas_Linha_X = int(Entrada[linha])
    linha += 1
    if Cargas_Linha_X == 0:
        #print "Não há cargas de linha paralelas a x."
        pass
    else:
        Cargas_Linha_X_V = []
        Cargas_Linha_X_VL = []
        for I in range(Cargas_Linha_X):
            Cargas_Linha_X_VL=Entrada[linha]
            Cargas_Linha_X_VL = Cargas_Linha_X_VL.split(" ")
            Cargas_Linha_X_VL[0] = int(Cargas_Linha_X_VL[0])  
            Cargas_Linha_X_VL[1] = float(Cargas_Linha_X_VL[1])
            Cargas_Linha_X_V.append(Cargas_Linha_X_VL)
            Cargas_Linha_X_VL =[]  
            linha += 1
        #print "Há cargas de linha paralelas a x: ", Cargas_Linha_X_V
            
    Cargas_Linha_Y = int(Entrada[linha])
    linha += 1
    if Cargas_Linha_Y == 0:
        #print "Não há cargas de linha paralelas a y."
        pass
    else:
        Cargas_Linha_Y_V = []
        Cargas_Linha_Y_VL = []
        for I in range(Cargas_Linha_Y):
            Cargas_Linha_Y_VL=Entrada[linha]
            Cargas_Linha_Y_VL = Cargas_Linha_Y_VL.split(" ")
            Cargas_Linha_Y_VL[0] = int(Cargas_Linha_Y_VL[0])  
            Cargas_Linha_Y_VL[1] = float(Cargas_Linha_Y_VL[1])
            Cargas_Linha_Y_V.append(Cargas_Linha_Y_VL)
            Cargas_Linha_Y_VL =[]  
            linha += 1
        #print "Há cargas de linha paralelas a y: ", Cargas_Linha_Y_V
        
    Cargas_Concentradas = int(Entrada[linha])
    linha += 1
    if Cargas_Concentradas == 0:
        pass
        #print "Não há cargas concentradas."
    else:
        Cargas_Concentradas_V = []
        for I in range(Cargas_Concentradas):
            Cargas_Concentradas_VL = Entrada[linha].split(" ")
            Cargas_Concentradas_VL[0] = int(Cargas_Concentradas_VL[0])
            for J in range(1,7): Cargas_Concentradas_VL[J] = float(Cargas_Concentradas_VL[J])
            Cargas_Concentradas_V.append(Cargas_Concentradas_VL)
            linha += 1
        #print "Há cargas concentradas: ", Cargas_Concentradas_V
        
    Restricoes_Bordo = []
    for I in range(4):
        Restricoes_BordoL = Entrada[linha].split(" ")
        Restricoes_BordoL = [int(i) for i in Restricoes_BordoL] # converter para inteiros
        Restricoes_Bordo.append(Restricoes_BordoL)
        linha += 1
    #print "Restrições de bordo", Restricoes_Bordo

    Restricoes_Canto = []
    for I in range(4):
        Restricoes_CantoL = Entrada[linha].split(" ")
        Restricoes_CantoL = [int(i) for i in Restricoes_CantoL] # converter para inteiros
        Restricoes_Canto.append(Restricoes_CantoL)
        linha += 1
    #print "Restrições de canto", Restricoes_Canto
    
    Restricoes_Nos = int(Entrada[linha])
    linha += 1
    if Restricoes_Nos == 0: 
        #print "Não há restricoes em nós específicos."
        pass
    else:
        Restricoes_Nos_V = []
        for I in range(Restricoes_Nos):
            Restricoes_Nos_VL = Entrada[linha].split(" ")
            Restricoes_Nos_VL = [int(i) for i in Restricoes_Nos_VL] # converter para inteiros
            Restricoes_Nos_V.append(Restricoes_Nos_VL)
            Restricoes_Nos_VL = []
            linha += 1
        #print "Há %d nós específicos restritos." %(Restricoes_Nos)
        #print Restricoes_Nos_V
        
def fun_calculos_iniciais():
    global NNx
    global NNy
    global NN
    global NE
    global a
    global b
    global Coordenadas
    global Incidencia
    global Lam_C
    global Banda
    

    # Número de elementos e nós
    NE = NEx*NEy
    NNx,NNy = NEx+1,NEy+1
    NN = NNx*NNy
    
    # Definir o tamanho da banda
    Banda = 6*(NNx+2)
        
    # Tamanho dos elementos
    a = La/NEx
    b = Lb/NEy
    
    # Coordenadas
    x = np.linspace(0,La,NNx)
    y = np.linspace(0,Lb,NNy)
    
    Coordenadas = []
    CoordenadasL = []
    n_no = 1
    for J in range(NNx):
        for I in range (NNy):
            CoordenadasL.append(n_no)
            CoordenadasL.append(x[I])
            CoordenadasL.append(y[J])
            CoordenadasL.append(0)
            n_no += 1
            Coordenadas.append(CoordenadasL)
            CoordenadasL = []
    
    # Incidência
    IncidenciaL = []
    Incidencia = []
    for I in range(1,NEy+1):
        for J in range(1,NEx+1):
            IncidenciaL.append((J-1)+(I-1)*NNx+1)
            IncidenciaL.append(IncidenciaL[0]+1)
            IncidenciaL.append(IncidenciaL[0]+NNx)
            IncidenciaL.append(IncidenciaL[2]+1)
            Incidencia.append(IncidenciaL)
            IncidenciaL = []
    
    # Calcular a espessura total da placa
    ht = 0.0
    for Lamina in Laminas:
        ht += float(Lamina[1])
    
    # Matriz completa de propriedades das lâminas
    Lam_C = []
    Lam_CL = []
    cont_lam = 0
    for Lamina in Laminas:
        material = int(Lamina[0])
        E1 = float(Materiais[material-1][2])
        E2 = float(Materiais[material-1][3])
        G12 = float(Materiais[material-1][4])
        v12 = float(Materiais[material-1][5])
        v21 = v12*E2/E1
        teta = float(Lamina[2])
        
        if cont_lam == 0:
            hi = -ht/2
            hf = hi+float(Lamina[1]) #-ht/2+espessura
            hm = (hi+hf)/2
        else:
            hi = Lam_C[cont_lam-1][7]
            hf = hi+float(Lamina[1])
            hm = (hi+hf)/2
            
        Lam_CL.append(E1)
        Lam_CL.append(E2)
        Lam_CL.append(G12)
        Lam_CL.append(v12)
        Lam_CL.append(v21)
        Lam_CL.append(teta)
        Lam_CL.append(hi)
        Lam_CL.append(hf)
        Lam_CL.append(hm)
        
        Lam_C.append(Lam_CL)
        Lam_CL = []
        cont_lam += 1    
        

def fun_matriz_constitutiva():
    global A
    global B
    global D
    global Prop_Lam
    
    Prop_Lam = []
    Prop_LamL = []
    for Lamina in Lam_C:
        E1 = Lamina[0]
        E2 = Lamina[1]
        G12 = Lamina[2]
        v12 = Lamina[3]
        v21 = Lamina[4]
        teta = math.radians(Lamina[5]) #convertendo o ângulo pra radianos
        
        Q11 = E1/(1-v12*v21)
        Q12 = v12*E2/(1-v12*v21)
        Q22 = E2/(1-v12*v21)
        Q66 = G12
    
        Q11_R = Q11*(math.cos(teta))**4+2*(Q12+2*Q66)*(math.sin(teta))**2*(math.cos(teta))**2+Q22*(math.sin(teta))**4
        Q12_R = (Q11+Q22-4*Q66)*(math.sin(teta))**2*(math.cos(teta))**2+Q12*((math.sin(teta))**4+(math.cos(teta))**4)
        Q16_R = (Q11-Q12-2*Q66)*math.sin(teta)*(math.cos(teta))**3+(Q12-Q22+2*Q66)*math.cos(teta)*(math.sin(teta))**3
        Q22_R = Q11*(math.sin(teta))**4+2*(Q12+2*Q66)*(math.sin(teta))**2*(math.cos(teta))**2+Q22*(math.cos(teta))**4
        Q26_R = (Q11-Q12-2*Q66)*(math.sin(teta))**3*(math.cos(teta))+(Q12-Q22+2*Q66)*math.sin(teta)*(math.cos(teta))**3
        Q66_R = (Q11+Q22-2*Q12-2*Q66)*(math.sin(teta))**2*(math.cos(teta))**2+Q66*((math.sin(teta))**4+(math.cos(teta))**4)
        
        Prop_LamL.append(Q11_R)
        Prop_LamL.append(Q12_R)
        Prop_LamL.append(Q16_R)
        Prop_LamL.append(Q22_R)
        Prop_LamL.append(Q26_R)
        Prop_LamL.append(Q66_R)
        
        Prop_Lam.append(Prop_LamL)
        Prop_LamL = []
    
    
    # Uniformizar a seção
    A11,A12,A16,A22,A26,A66 = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    B11,B12,B16,B22,B26,B66 = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    D11,D12,D16,D22,D26,D66 = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    
    for I in range(Num_Laminas):
        #Aij = Aij+Qij*(z1-z0)
        A11 += Prop_Lam[I][0]*(Lam_C[I][7]-Lam_C[I][6])    
        A12 += Prop_Lam[I][1]*(Lam_C[I][7]-Lam_C[I][6])    
        A16 += Prop_Lam[I][2]*(Lam_C[I][7]-Lam_C[I][6])    
        A22 += Prop_Lam[I][3]*(Lam_C[I][7]-Lam_C[I][6])    
        A26 += Prop_Lam[I][4]*(Lam_C[I][7]-Lam_C[I][6])    
        A66 += Prop_Lam[I][5]*(Lam_C[I][7]-Lam_C[I][6])    
        
        #Bij = Bij+Qij(z1^2-z0^2)
        B11 += Prop_Lam[I][0]*((Lam_C[I][7])**2-(Lam_C[I][6])**2)    
        B12 += Prop_Lam[I][1]*((Lam_C[I][7])**2-(Lam_C[I][6])**2)    
        B16 += Prop_Lam[I][2]*((Lam_C[I][7])**2-(Lam_C[I][6])**2)    
        B22 += Prop_Lam[I][3]*((Lam_C[I][7])**2-(Lam_C[I][6])**2)    
        B26 += Prop_Lam[I][4]*((Lam_C[I][7])**2-(Lam_C[I][6])**2)    
        B66 += Prop_Lam[I][5]*((Lam_C[I][7])**2-(Lam_C[I][6])**2)    
    
        #Dij = Dij+Qij(z1^3-z0^3)
        D11 += Prop_Lam[I][0]*((Lam_C[I][7])**3-(Lam_C[I][6])**3)    
        D12 += Prop_Lam[I][1]*((Lam_C[I][7])**3-(Lam_C[I][6])**3)    
        D16 += Prop_Lam[I][2]*((Lam_C[I][7])**3-(Lam_C[I][6])**3)    
        D22 += Prop_Lam[I][3]*((Lam_C[I][7])**3-(Lam_C[I][6])**3)    
        D26 += Prop_Lam[I][4]*((Lam_C[I][7])**3-(Lam_C[I][6])**3)    
        D66 += Prop_Lam[I][5]*((Lam_C[I][7])**3-(Lam_C[I][6])**3)    
    
    B11,B12,B16,B22,B26,B66 = B11/2,B12/2,B16/2,B22/2,B26/2,B66/2
    D11,D12,D16,D22,D26,D66 = D11/3,D12/3,D16/3,D22/3,D26/3,D66/3
    
    A = [A11,A12,A16,A22,A26,A66]
    B = [B11,B12,B16,B22,B26,B66]
    D = [D11,D12,D16,D22,D26,D66]
    

def fun_rigidez_local():
    global RigidezL
    
    A11,A12,A16,A22,A26,A66 = A[0],A[1],A[2],A[3],A[4],A[5]
    B11,B12,B16,B22,B26,B66 = B[0],B[1],B[2],B[3],B[4],B[5]
    D11,D12,D16,D22,D26,D66 = D[0],D[1],D[2],D[3],D[4],D[5]
    
    RigidezL_L = []
    RigidezL = []
    for I in range(24):
        for J in range(24):
            RigidezL_L.append(0)
        RigidezL.append(RigidezL_L)
        RigidezL_L=[]
    
    RigidezL[0][0]=(2*A66*a**2+2*A11*b**2+3*A16*a*b)/(6*a*b)
    RigidezL[0][1]=(4*A26*a**2+4*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[0][2]=-(B12*a-2*B66*a-B16*b)/(2*a*b)
    RigidezL[0][3]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2+30*B16*a*b)/(60*a*b)
    RigidezL[0][4]=-(7*B26*a**2+5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[0][5]=-(3*B26*a**3+3*B11*b**3+5*B12*a**2*b+5*B16*a*b**2)/(60*a*b)
    RigidezL[0][6]=-(2*A11*b**2-A66*a**2)/(6*a*b)
    RigidezL[0][7]=(2*A26*a**2-4*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[0][8]=-(B12*a+2*B66*a+B16*b)/(2*a*b)
    RigidezL[0][9]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[0][10]=-(3*B26*a**2-5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[0][11]=(2*B26*a**3+3*B11*b**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[0][12]=(A11*b**2-2*A66*a**2)/(6*a*b)
    RigidezL[0][13]=-(4*A26*a**2-2*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[0][14]=(B12*a-2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[0][15]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2+30*B16*a*b)/(60*a*b)
    RigidezL[0][16]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[0][17]=(3*B26*a**3+2*B11*b**3+5*B16*a*b**2)/(60*a*b)
    RigidezL[0][18]=-(A66*a**2+A11*b**2+3*A16*a*b)/(6*a*b)
    RigidezL[0][19]=-(2*A26*a**2+2*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[0][20]=(B12*a+2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[0][21]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[0][22]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[0][23]=-(B26*a**3+B11*b**3)/(30*a*b)
    RigidezL[1][0]=(4*A26*a**2+4*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[1][1]=(2*A22*a**2+2*A66*b**2+3*A26*a*b)/(6*a*b)
    RigidezL[1][2]=-(B12*b-B26*a-2*B66*b)/(2*a*b)
    RigidezL[1][3]=-(5*B26*a**2+7*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[1][4]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[1][5]=-(3*B22*a**3+3*B16*b**3+5*B12*a*b**2+5*B26*a**2*b)/(60*a*b)
    RigidezL[1][6]=-(4*A16*b**2-2*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[1][7]=(A22*a**2-2*A66*b**2)/(6*a*b)
    RigidezL[1][8]=(B12*b-3*B26*a-2*B66*b)/(2*a*b)
    RigidezL[1][9]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[1][10]=(5*B12*b**2-9*B22*a**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[1][11]=(2*B22*a**3+3*B16*b**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[1][12]=(2*A16*b**2-4*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[1][13]=-(2*A22*a**2-A66*b**2)/(6*a*b)
    RigidezL[1][14]=-(B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[1][15]=-(3*B16*b**2-5*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[1][16]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[1][17]=(3*B22*a**3+2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[1][18]=-(2*A26*a**2+2*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[1][19]=-(A22*a**2+A66*b**2+3*A26*a*b)/(6*a*b)
    RigidezL[1][20]=(3*B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[1][21]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[1][22]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[1][23]=-(B22*a**3+B16*b**3)/(30*a*b)
    RigidezL[2][0]=-(B12*a-2*B66*a-B16*b)/(2*a*b)
    RigidezL[2][1]=-(B12*b-B26*a-2*B66*b)/(2*a*b)
    RigidezL[2][2]=(12*(65*D22*a**4+65*D11*b**4+42*D12*a**2*b**2+84*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[2][3]=(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[2][4]=(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[2][5]=(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[2][6]=(B12*a+2*B66*a-B16*b)/(2*a*b)
    RigidezL[2][7]=(3*B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[2][8]=-(6*(130*D11*b**4-45*D22*a**4+84*D12*a**2*b**2+168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[2][9]=(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[2][10]=-(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[2][11]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[2][12]=(B12*a-2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[2][13]=(B12*b-B26*a+2*B66*b)/(2*a*b)
    RigidezL[2][14]=(6*(45*D11*b**4-130*D22*a**4-84*D12*a**2*b**2-168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[2][15]=(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[2][16]=-(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[2][17]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[2][18]=-(B12*a+2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[2][19]=-(3*B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[2][20]=-(18*(15*D22*a**4+15*D11*b**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[2][21]=(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[2][22]=(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[2][23]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[3][0]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2+30*B16*a*b)/(60*a*b)
    RigidezL[3][1]=-(5*B26*a**2+7*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[3][2]=(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[3][3]=(20*D22*a**4+260*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2+175*D16*a*b**3)/(175*a*b**3)
    RigidezL[3][4]=(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[3][5]=(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[3][6]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[3][7]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[3][8]=-(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[3][9]=(130*D11*b**4-15*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(175*a*b**3)
    RigidezL[3][10]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[3][11]=(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[3][12]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2-30*B16*a*b)/(60*a*b)
    RigidezL[3][13]=(5*B26*a**2-3*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[3][14]=(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[3][15]=(2*(45*D11*b**4-10*D22*a**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a*b**3)
    RigidezL[3][16]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[3][17]=-(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[3][18]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[3][19]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[3][20]=-(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[3][21]=(15*D22*a**4+45*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2+175*D16*a*b**3)/(175*a*b**3)
    RigidezL[3][22]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[3][23]=-(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[4][0]=-(7*B26*a**2+5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[4][1]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[4][2]=(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[4][3]=(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[4][4]=(260*D22*a**4+20*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2+175*D26*a**3*b)/(175*a**3*b)
    RigidezL[4][5]=(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[4][6]=(5*B16*b**2-3*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[4][7]=(5*B12*b**2-9*B22*a**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[4][8]=-(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[4][9]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[4][10]=-(2*(10*D11*b**4-45*D22*a**4+28*D12*a**2*b**2+56*D66*a**2*b**2))/(175*a**3*b)
    RigidezL[4][11]=(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[4][12]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[4][13]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[4][14]=(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[4][15]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[4][16]=-(15*D11*b**4-130*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(175*a**3*b)
    RigidezL[4][17]=-(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[4][18]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[4][19]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[4][20]=-(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[4][21]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[4][22]=(45*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2+175*D26*a**3*b)/(175*a**3*b)
    RigidezL[4][23]=-(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[5][0]=-(3*B26*a**3+3*B11*b**3+5*B12*a**2*b+5*B16*a*b**2)/(60*a*b)
    RigidezL[5][1]=-(3*B22*a**3+3*B16*b**3+5*B12*a*b**2+5*B26*a**2*b)/(60*a*b)
    RigidezL[5][2]=(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[5][3]=(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[5][4]=(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[5][5]=(4*(15*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2))/(1575*a*b)
    RigidezL[5][6]=(3*B11*b**3-2*B26*a**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[5][7]=(3*B16*b**3-2*B22*a**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[5][8]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[5][9]=(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[5][10]=-(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[5][11]=(30*D11*b**4-45*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[5][12]=-(2*B11*b**3-3*B26*a**3-5*B16*a*b**2)/(60*a*b)
    RigidezL[5][13]=(3*B22*a**3-2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[5][14]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[5][15]=(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[5][16]=-(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[5][17]=-(45*D11*b**4-30*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[5][18]=(B26*a**3+B11*b**3)/(30*a*b)
    RigidezL[5][19]=(B22*a**3+B16*b**3)/(30*a*b)
    RigidezL[5][20]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[5][21]=(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[5][22]=(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[5][23]=-(45*D22*a**4+45*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+105*D16*a*b**3+105*D26*a**3*b)/(3150*a*b)
    RigidezL[6][0]=-(2*A11*b**2-A66*a**2)/(6*a*b)
    RigidezL[6][1]=-(4*A16*b**2-2*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[6][2]=(B12*a+2*B66*a-B16*b)/(2*a*b)
    RigidezL[6][3]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[6][4]=(5*B16*b**2-3*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[6][5]=(3*B11*b**3-2*B26*a**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[6][6]=(2*A66*a**2+2*A11*b**2-3*A16*a*b)/(6*a*b)
    RigidezL[6][7]=-(3*A12*a*b-4*A16*b**2-4*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[6][8]=(B12*a-2*B66*a+B16*b)/(2*a*b)
    RigidezL[6][9]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2-30*B16*a*b)/(60*a*b)
    RigidezL[6][10]=(10*B12*a*b-5*B16*b**2-7*B26*a**2)/(20*a*b)
    RigidezL[6][11]=-(3*B11*b**3-3*B26*a**3+5*B12*a**2*b-5*B16*a*b**2)/(60*a*b)
    RigidezL[6][12]=-(A66*a**2+A11*b**2-3*A16*a*b)/(6*a*b)
    RigidezL[6][13]=(3*A12*a*b-2*A16*b**2-2*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[6][14]=-(B12*a+2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[6][15]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[6][16]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[6][17]=-(B11*b**3-B26*a**3)/(30*a*b)
    RigidezL[6][18]=(A11*b**2-2*A66*a**2)/(6*a*b)
    RigidezL[6][19]=(2*A16*b**2-4*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[6][20]=-(B12*a-2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[6][21]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2-30*B16*a*b)/(60*a*b)
    RigidezL[6][22]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[6][23]=(2*B11*b**3-3*B26*a**3-5*B16*a*b**2)/(60*a*b)
    RigidezL[7][0]=(2*A26*a**2-4*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[7][1]=(A22*a**2-2*A66*b**2)/(6*a*b)
    RigidezL[7][2]=(3*B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[7][3]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[7][4]=(5*B12*b**2-9*B22*a**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[7][5]=(3*B16*b**3-2*B22*a**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[7][6]=-(3*A12*a*b-4*A16*b**2-4*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[7][7]=(2*A22*a**2+2*A66*b**2-3*A26*a*b)/(6*a*b)
    RigidezL[7][8]=-(B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[7][9]=(10*B12*a*b-7*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[7][10]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[7][11]=(3*B22*a**3-3*B16*b**3+5*B12*a*b**2-5*B26*a**2*b)/(60*a*b)
    RigidezL[7][12]=(3*A12*a*b-2*A16*b**2-2*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[7][13]=-(A22*a**2+A66*b**2-3*A26*a*b)/(6*a*b)
    RigidezL[7][14]=(B12*b-3*B26*a+2*B66*b)/(2*a*b)
    RigidezL[7][15]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[7][16]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[7][17]=-(B16*b**3-B22*a**3)/(30*a*b)
    RigidezL[7][18]=-(4*A26*a**2-2*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[7][19]=-(2*A22*a**2-A66*b**2)/(6*a*b)
    RigidezL[7][20]=-(B12*b-B26*a+2*B66*b)/(2*a*b)
    RigidezL[7][21]=(5*B26*a**2-3*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[7][22]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[7][23]=-(3*B22*a**3-2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[8][0]=-(B12*a+2*B66*a+B16*b)/(2*a*b)
    RigidezL[8][1]=(B12*b-3*B26*a-2*B66*b)/(2*a*b)
    RigidezL[8][2]=-(6*(130*D11*b**4-45*D22*a**4+84*D12*a**2*b**2+168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[8][3]=-(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[8][4]=-(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[8][5]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[8][6]=(B12*a-2*B66*a+B16*b)/(2*a*b)
    RigidezL[8][7]=-(B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[8][8]=(12*(65*D22*a**4+65*D11*b**4+42*D12*a**2*b**2+84*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[8][9]=-(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[8][10]=(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[8][11]=-(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[8][12]=(B12*a+2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[8][13]=-(B12*b-3*B26*a+2*B66*b)/(2*a*b)
    RigidezL[8][14]=-(18*(15*D22*a**4+15*D11*b**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[8][15]=-(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[8][16]=(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[8][17]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[8][18]=-(B12*a-2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[8][19]=(B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[8][20]=(6*(45*D11*b**4-130*D22*a**4-84*D12*a**2*b**2-168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[8][21]=-(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[8][22]=-(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[8][23]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[9][0]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[9][1]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[9][2]=(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[9][3]=(130*D11*b**4-15*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(175*a*b**3)
    RigidezL[9][4]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[9][5]=(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[9][6]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2-30*B16*a*b)/(60*a*b)
    RigidezL[9][7]=(10*B12*a*b-7*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[9][8]=-(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[9][9]=(20*D22*a**4+260*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2-175*D16*a*b**3)/(175*a*b**3)
    RigidezL[9][10]=-(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[9][11]=(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[9][12]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[9][13]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[9][14]=(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[9][15]=(15*D22*a**4+45*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2-175*D16*a*b**3)/(175*a*b**3)
    RigidezL[9][16]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[9][17]=-(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[9][18]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2+30*B16*a*b)/(60*a*b)
    RigidezL[9][19]=-(3*B16*b**2-5*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[9][20]=-(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[9][21]=(2*(45*D11*b**4-10*D22*a**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a*b**3)
    RigidezL[9][22]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[9][23]=-(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[10][0]=-(3*B26*a**2-5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[10][1]=(5*B12*b**2-9*B22*a**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[10][2]=-(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[10][3]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[10][4]=-(2*(10*D11*b**4-45*D22*a**4+28*D12*a**2*b**2+56*D66*a**2*b**2))/(175*a**3*b)
    RigidezL[10][5]=-(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[10][6]=(10*B12*a*b-5*B16*b**2-7*B26*a**2)/(20*a*b)
    RigidezL[10][7]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[10][8]=(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[10][9]=-(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[10][10]=(260*D22*a**4+20*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2-175*D26*a**3*b)/(175*a**3*b)
    RigidezL[10][11]=-(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[10][12]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[10][13]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[10][14]=-(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[10][15]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[10][16]=(45*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2-175*D26*a**3*b)/(175*a**3*b)
    RigidezL[10][17]=(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[10][18]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[10][19]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[10][20]=(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[10][21]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[10][22]=-(15*D11*b**4-130*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(175*a**3*b)
    RigidezL[10][23]=(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[11][0]=(2*B26*a**3+3*B11*b**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[11][1]=(2*B22*a**3+3*B16*b**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[11][2]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[11][3]=(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[11][4]=(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[11][5]=(30*D11*b**4-45*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[11][6]=-(3*B11*b**3-3*B26*a**3+5*B12*a**2*b-5*B16*a*b**2)/(60*a*b)
    RigidezL[11][7]=(3*B22*a**3-3*B16*b**3+5*B12*a*b**2-5*B26*a**2*b)/(60*a*b)
    RigidezL[11][8]=-(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[11][9]=(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[11][10]=-(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[11][11]=(4*(15*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2))/(1575*a*b)
    RigidezL[11][12]=(B11*b**3-B26*a**3)/(30*a*b)
    RigidezL[11][13]=(B16*b**3-B22*a**3)/(30*a*b)
    RigidezL[11][14]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[11][15]=(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[11][16]=-(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[11][17]=-(45*D22*a**4+45*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-105*D16*a*b**3-105*D26*a**3*b)/(3150*a*b)
    RigidezL[11][18]=-(3*B26*a**3+2*B11*b**3+5*B16*a*b**2)/(60*a*b)
    RigidezL[11][19]=-(3*B22*a**3+2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[11][20]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[11][21]=(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[11][22]=(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[11][23]=-(45*D11*b**4-30*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[12][0]=(A11*b**2-2*A66*a**2)/(6*a*b)
    RigidezL[12][1]=(2*A16*b**2-4*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[12][2]=(B12*a-2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[12][3]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2-30*B16*a*b)/(60*a*b)
    RigidezL[12][4]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[12][5]=-(2*B11*b**3-3*B26*a**3-5*B16*a*b**2)/(60*a*b)
    RigidezL[12][6]=-(A66*a**2+A11*b**2-3*A16*a*b)/(6*a*b)
    RigidezL[12][7]=(3*A12*a*b-2*A16*b**2-2*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[12][8]=(B12*a+2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[12][9]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[12][10]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[12][11]=(B11*b**3-B26*a**3)/(30*a*b)
    RigidezL[12][12]=(2*A66*a**2+2*A11*b**2-3*A16*a*b)/(6*a*b)
    RigidezL[12][13]=-(3*A12*a*b-4*A16*b**2-4*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[12][14]=-(B12*a-2*B66*a+B16*b)/(2*a*b)
    RigidezL[12][15]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2-30*B16*a*b)/(60*a*b)
    RigidezL[12][16]=(10*B12*a*b-5*B16*b**2-7*B26*a**2)/(20*a*b)
    RigidezL[12][17]=(3*B11*b**3-3*B26*a**3+5*B12*a**2*b-5*B16*a*b**2)/(60*a*b)
    RigidezL[12][18]=-(2*A11*b**2-A66*a**2)/(6*a*b)
    RigidezL[12][19]=-(4*A16*b**2-2*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[12][20]=-(B12*a+2*B66*a-B16*b)/(2*a*b)
    RigidezL[12][21]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[12][22]=(5*B16*b**2-3*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[12][23]=-(3*B11*b**3-2*B26*a**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[13][0]=-(4*A26*a**2-2*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[13][1]=-(2*A22*a**2-A66*b**2)/(6*a*b)
    RigidezL[13][2]=(B12*b-B26*a+2*B66*b)/(2*a*b)
    RigidezL[13][3]=(5*B26*a**2-3*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[13][4]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[13][5]=(3*B22*a**3-2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[13][6]=(3*A12*a*b-2*A16*b**2-2*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[13][7]=-(A22*a**2+A66*b**2-3*A26*a*b)/(6*a*b)
    RigidezL[13][8]=-(B12*b-3*B26*a+2*B66*b)/(2*a*b)
    RigidezL[13][9]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[13][10]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[13][11]=(B16*b**3-B22*a**3)/(30*a*b)
    RigidezL[13][12]=-(3*A12*a*b-4*A16*b**2-4*A26*a**2+3*A66*a*b)/(12*a*b)
    RigidezL[13][13]=(2*A22*a**2+2*A66*b**2-3*A26*a*b)/(6*a*b)
    RigidezL[13][14]=(B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[13][15]=(10*B12*a*b-7*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[13][16]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[13][17]=-(3*B22*a**3-3*B16*b**3+5*B12*a*b**2-5*B26*a**2*b)/(60*a*b)
    RigidezL[13][18]=(2*A26*a**2-4*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[13][19]=(A22*a**2-2*A66*b**2)/(6*a*b)
    RigidezL[13][20]=-(3*B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[13][21]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[13][22]=(5*B12*b**2-9*B22*a**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[13][23]=-(3*B16*b**3-2*B22*a**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[14][0]=(B12*a-2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[14][1]=-(B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[14][2]=(6*(45*D11*b**4-130*D22*a**4-84*D12*a**2*b**2-168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[14][3]=(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[14][4]=(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[14][5]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[14][6]=-(B12*a+2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[14][7]=(B12*b-3*B26*a+2*B66*b)/(2*a*b)
    RigidezL[14][8]=-(18*(15*D22*a**4+15*D11*b**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[14][9]=(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[14][10]=-(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[14][11]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[14][12]=-(B12*a-2*B66*a+B16*b)/(2*a*b)
    RigidezL[14][13]=(B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[14][14]=(12*(65*D22*a**4+65*D11*b**4+42*D12*a**2*b**2+84*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[14][15]=(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[14][16]=-(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[14][17]=-(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[14][18]=(B12*a+2*B66*a+B16*b)/(2*a*b)
    RigidezL[14][19]=-(B12*b-3*B26*a-2*B66*b)/(2*a*b)
    RigidezL[14][20]=-(6*(130*D11*b**4-45*D22*a**4+84*D12*a**2*b**2+168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[14][21]=(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[14][22]=(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[14][23]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[15][0]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2+30*B16*a*b)/(60*a*b)
    RigidezL[15][1]=-(3*B16*b**2-5*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[15][2]=(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[15][3]=(2*(45*D11*b**4-10*D22*a**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a*b**3)
    RigidezL[15][4]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[15][5]=(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[15][6]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[15][7]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[15][8]=-(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[15][9]=(15*D22*a**4+45*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2-175*D16*a*b**3)/(175*a*b**3)
    RigidezL[15][10]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[15][11]=(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[15][12]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2-30*B16*a*b)/(60*a*b)
    RigidezL[15][13]=(10*B12*a*b-7*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[15][14]=(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[15][15]=(20*D22*a**4+260*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2-175*D16*a*b**3)/(175*a*b**3)
    RigidezL[15][16]=-(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[15][17]=-(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[15][18]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[15][19]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[15][20]=-(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[15][21]=(130*D11*b**4-15*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(175*a*b**3)
    RigidezL[15][22]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[15][23]=-(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[16][0]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[16][1]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[16][2]=-(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[16][3]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[16][4]=-(15*D11*b**4-130*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(175*a**3*b)
    RigidezL[16][5]=-(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[16][6]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[16][7]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[16][8]=(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[16][9]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[16][10]=(45*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2-175*D26*a**3*b)/(175*a**3*b)
    RigidezL[16][11]=-(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[16][12]=(10*B12*a*b-5*B16*b**2-7*B26*a**2)/(20*a*b)
    RigidezL[16][13]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[16][14]=-(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[16][15]=-(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[16][16]=(260*D22*a**4+20*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2-175*D26*a**3*b)/(175*a**3*b)
    RigidezL[16][17]=(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[16][18]=-(3*B26*a**2-5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[16][19]=(5*B12*b**2-9*B22*a**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[16][20]=(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[16][21]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[16][22]=-(2*(10*D11*b**4-45*D22*a**4+28*D12*a**2*b**2+56*D66*a**2*b**2))/(175*a**3*b)
    RigidezL[16][23]=(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[17][0]=(3*B26*a**3+2*B11*b**3+5*B16*a*b**2)/(60*a*b)
    RigidezL[17][1]=(3*B22*a**3+2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[17][2]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[17][3]=-(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[17][4]=-(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[17][5]=-(45*D11*b**4-30*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[17][6]=-(B11*b**3-B26*a**3)/(30*a*b)
    RigidezL[17][7]=-(B16*b**3-B22*a**3)/(30*a*b)
    RigidezL[17][8]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[17][9]=-(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[17][10]=(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[17][11]=-(45*D22*a**4+45*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2-105*D16*a*b**3-105*D26*a**3*b)/(3150*a*b)
    RigidezL[17][12]=(3*B11*b**3-3*B26*a**3+5*B12*a**2*b-5*B16*a*b**2)/(60*a*b)
    RigidezL[17][13]=-(3*B22*a**3-3*B16*b**3+5*B12*a*b**2-5*B26*a**2*b)/(60*a*b)
    RigidezL[17][14]=-(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[17][15]=-(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[17][16]=(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[17][17]=(4*(15*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2))/(1575*a*b)
    RigidezL[17][18]=-(2*B26*a**3+3*B11*b**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[17][19]=-(2*B22*a**3+3*B16*b**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[17][20]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[17][21]=-(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[17][22]=-(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[17][23]=(30*D11*b**4-45*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[18][0]=-(A66*a**2+A11*b**2+3*A16*a*b)/(6*a*b)
    RigidezL[18][1]=-(2*A26*a**2+2*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[18][2]=-(B12*a+2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[18][3]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[18][4]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[18][5]=(B26*a**3+B11*b**3)/(30*a*b)
    RigidezL[18][6]=(A11*b**2-2*A66*a**2)/(6*a*b)
    RigidezL[18][7]=-(4*A26*a**2-2*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[18][8]=-(B12*a-2*B66*a-3*B16*b)/(2*a*b)
    RigidezL[18][9]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2+30*B16*a*b)/(60*a*b)
    RigidezL[18][10]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[18][11]=-(3*B26*a**3+2*B11*b**3+5*B16*a*b**2)/(60*a*b)
    RigidezL[18][12]=-(2*A11*b**2-A66*a**2)/(6*a*b)
    RigidezL[18][13]=(2*A26*a**2-4*A16*b**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[18][14]=(B12*a+2*B66*a+B16*b)/(2*a*b)
    RigidezL[18][15]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[18][16]=-(3*B26*a**2-5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[18][17]=-(2*B26*a**3+3*B11*b**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[18][18]=(2*A66*a**2+2*A11*b**2+3*A16*a*b)/(6*a*b)
    RigidezL[18][19]=(4*A26*a**2+4*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[18][20]=(B12*a-2*B66*a-B16*b)/(2*a*b)
    RigidezL[18][21]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2+30*B16*a*b)/(60*a*b)
    RigidezL[18][22]=-(7*B26*a**2+5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[18][23]=(3*B26*a**3+3*B11*b**3+5*B12*a**2*b+5*B16*a*b**2)/(60*a*b)
    RigidezL[19][0]=-(2*A26*a**2+2*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[19][1]=-(A22*a**2+A66*b**2+3*A26*a*b)/(6*a*b)
    RigidezL[19][2]=-(3*B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[19][3]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[19][4]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[19][5]=(B22*a**3+B16*b**3)/(30*a*b)
    RigidezL[19][6]=(2*A16*b**2-4*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[19][7]=-(2*A22*a**2-A66*b**2)/(6*a*b)
    RigidezL[19][8]=(B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[19][9]=-(3*B16*b**2-5*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[19][10]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[19][11]=-(3*B22*a**3+2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[19][12]=-(4*A16*b**2-2*A26*a**2+3*A12*a*b-3*A66*a*b)/(12*a*b)
    RigidezL[19][13]=(A22*a**2-2*A66*b**2)/(6*a*b)
    RigidezL[19][14]=-(B12*b-3*B26*a-2*B66*b)/(2*a*b)
    RigidezL[19][15]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[19][16]=(5*B12*b**2-9*B22*a**2+10*B66*b**2-30*B26*a*b)/(60*a*b)
    RigidezL[19][17]=-(2*B22*a**3+3*B16*b**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[19][18]=(4*A26*a**2+4*A16*b**2+3*A12*a*b+3*A66*a*b)/(12*a*b)
    RigidezL[19][19]=(2*A22*a**2+2*A66*b**2+3*A26*a*b)/(6*a*b)
    RigidezL[19][20]=(B12*b-B26*a-2*B66*b)/(2*a*b)
    RigidezL[19][21]=-(5*B26*a**2+7*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[19][22]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[19][23]=(3*B22*a**3+3*B16*b**3+5*B12*a*b**2+5*B26*a**2*b)/(60*a*b)
    RigidezL[20][0]=(B12*a+2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[20][1]=(3*B26*a+B12*b+2*B66*b)/(2*a*b)
    RigidezL[20][2]=-(18*(15*D22*a**4+15*D11*b**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[20][3]=-(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[20][4]=-(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[20][5]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[20][6]=-(B12*a-2*B66*a+3*B16*b)/(2*a*b)
    RigidezL[20][7]=-(B12*b-B26*a+2*B66*b)/(2*a*b)
    RigidezL[20][8]=(6*(45*D11*b**4-130*D22*a**4-84*D12*a**2*b**2-168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[20][9]=-(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2-350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[20][10]=(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[20][11]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[20][12]=-(B12*a+2*B66*a-B16*b)/(2*a*b)
    RigidezL[20][13]=-(3*B26*a+B12*b-2*B66*b)/(2*a*b)
    RigidezL[20][14]=-(6*(130*D11*b**4-45*D22*a**4+84*D12*a**2*b**2+168*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[20][15]=-(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[20][16]=(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[20][17]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[20][18]=(B12*a-2*B66*a-B16*b)/(2*a*b)
    RigidezL[20][19]=(B12*b-B26*a-2*B66*b)/(2*a*b)
    RigidezL[20][20]=(12*(65*D22*a**4+65*D11*b**4+42*D12*a**2*b**2+84*D66*a**2*b**2))/(175*a**3*b**3)
    RigidezL[20][21]=-(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[20][22]=-(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[20][23]=(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[21][0]=(9*B11*b**2-10*B66*a**2-5*B12*a**2)/(60*a*b)
    RigidezL[21][1]=(3*B16*b**2-5*B26*a**2)/(20*a*b)
    RigidezL[21][2]=(65*D22*a**4+135*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[21][3]=(15*D22*a**4+45*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2+175*D16*a*b**3)/(175*a*b**3)
    RigidezL[21][4]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[21][5]=(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[21][6]=-(9*B11*b**2-10*B66*a**2-5*B12*a**2-30*B16*a*b)/(60*a*b)
    RigidezL[21][7]=(5*B26*a**2-3*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[21][8]=-(135*D11*b**4-110*D22*a**4-252*D12*a**2*b**2-84*D66*a**2*b**2+350*D16*a*b**3)/(175*a**2*b**3)
    RigidezL[21][9]=(2*(45*D11*b**4-10*D22*a**4-28*D12*a**2*b**2-56*D66*a**2*b**2))/(175*a*b**3)
    RigidezL[21][10]=-(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[21][11]=(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[21][12]=(5*B12*a**2+10*B66*a**2+21*B11*b**2)/(60*a*b)
    RigidezL[21][13]=(5*B26*a**2+7*B16*b**2)/(20*a*b)
    RigidezL[21][14]=(390*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+84*D66*a**2*b**2)/(175*a**2*b**3)
    RigidezL[21][15]=(130*D11*b**4-15*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(175*a*b**3)
    RigidezL[21][16]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[21][17]=-(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[21][18]=-(5*B12*a**2+10*B66*a**2+21*B11*b**2+30*B16*a*b)/(60*a*b)
    RigidezL[21][19]=-(5*B26*a**2+7*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[21][20]=-(2*(55*D22*a**4+195*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**2*b**3)
    RigidezL[21][21]=(20*D22*a**4+260*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2+175*D16*a*b**3)/(175*a*b**3)
    RigidezL[21][22]=(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[21][23]=-(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[22][0]=-(5*B16*b**2-3*B26*a**2)/(20*a*b)
    RigidezL[22][1]=-(5*B12*b**2-9*B22*a**2+10*B66*b**2)/(60*a*b)
    RigidezL[22][2]=(135*D22*a**4+65*D11*b**4-42*D12*a**2*b**2-84*D66*a**2*b**2+350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[22][3]=(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[22][4]=(45*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2+175*D26*a**3*b)/(175*a**3*b)
    RigidezL[22][5]=(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[22][6]=(7*B26*a**2+5*B16*b**2)/(20*a*b)
    RigidezL[22][7]=(21*B22*a**2+5*B12*b**2+10*B66*b**2)/(60*a*b)
    RigidezL[22][8]=-(65*D11*b**4-390*D22*a**4-42*D12*a**2*b**2-84*D66*a**2*b**2)/(175*a**3*b**2)
    RigidezL[22][9]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[22][10]=-(15*D11*b**4-130*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(175*a**3*b)
    RigidezL[22][11]=(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-70*D16*a*b**3-210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[22][12]=(5*B16*b**2-3*B26*a**2+10*B12*a*b)/(20*a*b)
    RigidezL[22][13]=(5*B12*b**2-9*B22*a**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[22][14]=(110*D11*b**4-135*D22*a**4+252*D12*a**2*b**2+84*D66*a**2*b**2-350*D26*a**3*b)/(175*a**3*b**2)
    RigidezL[22][15]=(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[22][16]=-(2*(10*D11*b**4-45*D22*a**4+28*D12*a**2*b**2+56*D66*a**2*b**2))/(175*a**3*b)
    RigidezL[22][17]=-(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[22][18]=-(7*B26*a**2+5*B16*b**2+10*B12*a*b)/(20*a*b)
    RigidezL[22][19]=-(21*B22*a**2+5*B12*b**2+10*B66*b**2+30*B26*a*b)/(60*a*b)
    RigidezL[22][20]=-(2*(195*D22*a**4+55*D11*b**4+126*D12*a**2*b**2+42*D66*a**2*b**2))/(175*a**3*b**2)
    RigidezL[22][21]=(110*D22*a**4+110*D11*b**4+427*D12*a**2*b**2+14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[22][22]=(260*D22*a**4+20*D11*b**4+56*D12*a**2*b**2+112*D66*a**2*b**2+175*D26*a**3*b)/(175*a**3*b)
    RigidezL[22][23]=-(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[23][0]=-(B26*a**3+B11*b**3)/(30*a*b)
    RigidezL[23][1]=-(B22*a**3+B16*b**3)/(30*a*b)
    RigidezL[23][2]=-(65*D22*a**4+65*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[23][3]=-(45*D22*a**4+65*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+210*D16*a*b**3+70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[23][4]=-(65*D22*a**4+45*D11*b**4+7*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[23][5]=-(45*D22*a**4+45*D11*b**4-7*D12*a**2*b**2-14*D66*a**2*b**2+105*D16*a*b**3+105*D26*a**3*b)/(3150*a*b)
    RigidezL[23][6]=(2*B11*b**3-3*B26*a**3-5*B16*a*b**2)/(60*a*b)
    RigidezL[23][7]=-(3*B22*a**3-2*B16*b**3+5*B12*a*b**2)/(60*a*b)
    RigidezL[23][8]=(65*D11*b**4-110*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2+140*D16*a*b**3+140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[23][9]=-(65*D11*b**4-30*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(525*a*b**2)
    RigidezL[23][10]=(45*D11*b**4-110*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2+70*D16*a*b**3+210*D26*a**3*b)/(1050*a**2*b)
    RigidezL[23][11]=-(45*D11*b**4-30*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[23][12]=-(3*B11*b**3-2*B26*a**3+5*B12*a**2*b)/(60*a*b)
    RigidezL[23][13]=-(3*B16*b**3-2*B22*a**3+5*B26*a**2*b)/(60*a*b)
    RigidezL[23][14]=-(110*D11*b**4-65*D22*a**4+42*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[23][15]=-(110*D11*b**4-45*D22*a**4-42*D12*a**2*b**2-14*D66*a**2*b**2-210*D16*a*b**3-70*D26*a**3*b)/(1050*a*b**2)
    RigidezL[23][16]=(30*D11*b**4-65*D22*a**4+14*D12*a**2*b**2+28*D66*a**2*b**2)/(525*a**2*b)
    RigidezL[23][17]=(30*D11*b**4-45*D22*a**4-14*D12*a**2*b**2-28*D66*a**2*b**2)/(1575*a*b)
    RigidezL[23][18]=(3*B26*a**3+3*B11*b**3+5*B12*a**2*b+5*B16*a*b**2)/(60*a*b)
    RigidezL[23][19]=(3*B22*a**3+3*B16*b**3+5*B12*a*b**2+5*B26*a**2*b)/(60*a*b)
    RigidezL[23][20]=(110*D22*a**4+110*D11*b**4+77*D12*a**2*b**2+14*D66*a**2*b**2-140*D16*a*b**3-140*D26*a**3*b)/(350*a**2*b**2)
    RigidezL[23][21]=-(2*(15*D22*a**4+55*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a*b**2)
    RigidezL[23][22]=-(2*(55*D22*a**4+15*D11*b**4+42*D12*a**2*b**2+14*D66*a**2*b**2))/(525*a**2*b)
    RigidezL[23][23]=(4*(15*D22*a**4+15*D11*b**4+14*D12*a**2*b**2+28*D66*a**2*b**2))/(1575*a*b)
    
        
def fun_rigidez_global():
    global Matriz
    global NGL
    
    NGL = 6*NN
    
    # Gerar a matriz com zeros
    MatrizL = []
    Matriz = []
    for I in range(2*Banda-1):
        for J in range(NGL):
            MatrizL.append(0.0)
        Matriz.append(MatrizL)
        MatrizL = []
    
    
    # GERACAO DA MATRIZ DE RIGIDEZ GLOBAL A PARTIR DA RIGIDEZ LOCAL
    NGLN=6 #número de graus de liberdade por nó
    NNE=4 #número nós por elementos

    for cont in range(NE): #ao invés de 1 a NE, de 0 a NE-1
        for I in range(NNE): # de 0 a NNE-1
            no1=Incidencia[cont][I]
            #print "\n%d " %(no1),
            Linhai=NGLN*(no1-1) # Tirei o +1, a linha começa em 0
            for J in range(NNE): # de 0 a NNE-1
                no2 = Incidencia[cont][J]
                #print "%d " %(no2),
                Linha=Linhai
                Colunai=NGLN*(no2-1) # Tirei o +1, a linha começa em 0
                Coluna=Colunai
                for K in range(NGLN):
                    for L in range(NGLN):
                        #Matriz[Linha][Coluna] += RigidezL[NGLN*(I-1)+K][NGLN*(J-1)+L]
                        #Matriz[Linha][Coluna] += RigidezL[NGLN*I+K][NGLN*J+L] # Sem o -1, a contagem começa em zero 
                        Matriz[Linha-Coluna+Banda-1][Coluna] += RigidezL[NGLN*I+K][NGLN*J+L] # Armazenamento compacto 
                        Coluna += 1
                    Linha += 1
                    Coluna = Colunai
                

def fun_vetor_carga():
    global Vetor_Carga

    Vetor_Carga = [0]*NGL
        
    # Carga distribuída
    if Carga_Distribuida == 1:
        q = Carga_Distribuida_V
        
        for I in range(NE):
            no1 = Incidencia[I][0]
            no2 = Incidencia[I][1]
            no3 = Incidencia[I][2]
            no4 = Incidencia[I][3]
                    
            Vetor_Carga[6*no1-4] += q*a*b/4
            Vetor_Carga[6*no1-3] += q*a**2*b/24
            Vetor_Carga[6*no1-2] += q*a*b**2/24
            Vetor_Carga[6*no1-1] += q*a**2*b**2/144
            Vetor_Carga[6*no2-4] += q*a*b/4
            Vetor_Carga[6*no2-3] += -q*a**2*b/24
            Vetor_Carga[6*no2-2] += q*a*b**2/24
            Vetor_Carga[6*no2-1] += -q*a**2*b**2/144
            Vetor_Carga[6*no3-4] += q*a*b/4
            Vetor_Carga[6*no3-3] += q*a**2*b/24
            Vetor_Carga[6*no3-2] += -q*a*b**2/24
            Vetor_Carga[6*no3-1] += -q*a**2*b**2/144
            Vetor_Carga[6*no4-4] += q*a*b/4
            Vetor_Carga[6*no4-3] += -q*a**2*b/24
            Vetor_Carga[6*no4-2] += -q*a*b**2/24
            Vetor_Carga[6*no4-1] += q*a**2*b**2/144
    
    if Carga_Trapezoidal == 1:
        qt1 = Carga_Trapezoidal_V[0]
        qt2 = Carga_Trapezoidal_V[1]
        
        nelm = 0
        for J in range(NEy):
            for I in range(NEx):
                x1 = I*a
                x2 = (I+1)*a
                
                q1 = qt1+(qt2-qt1)*x1/La
                q2 = qt1+(qt2-qt1)*x2/La
                
                no1 = Incidencia[nelm][0]
                no2 = Incidencia[nelm][1]
                no3 = Incidencia[nelm][2]
                no4 = Incidencia[nelm][3]
                
                Vetor_Carga[6*no1-4] +=  (7*q1+3*q2)*a*b/40
                Vetor_Carga[6*no1-3] +=  (3*q1+2*q2)*a**2*b/120
                Vetor_Carga[6*no1-2] +=  (7*q1+3*q2)*a*b**2/240
                Vetor_Carga[6*no1-1] +=  (3*q1+2*q2)*a**2*b**2/720
                Vetor_Carga[6*no2-4] +=  (3*q1+7*q2)*a*b/40
                Vetor_Carga[6*no2-3] += -(2*q1+3*q2)*a**2*b/120
                Vetor_Carga[6*no2-2] +=  (3*q1+7*q2)*a*b**2/240
                Vetor_Carga[6*no2-1] += -(2*q1+3*q2)*a**2*b**2/720
                Vetor_Carga[6*no3-4] +=  (7*q1+3*q2)*a*b/40
                Vetor_Carga[6*no3-3] +=  (3*q1+2*q2)*a**2*b/120
                Vetor_Carga[6*no3-2] += -(7*q1+3*q2)*a*b**2/240
                Vetor_Carga[6*no3-1] += -(3*q1+2*q2)*a**2*b**2/720
                Vetor_Carga[6*no4-4] +=  (3*q1+7*q2)*a*b/40
                Vetor_Carga[6*no4-3] += -(2*q1+3*q2)*a**2*b/120
                Vetor_Carga[6*no4-2] += -(3*q1+7*q2)*a*b**2/240
                Vetor_Carga[6*no4-1] +=  (2*q1+3*q2)*a**2*b**2/720
            
                nelm += 1 #próximo elemento
    
    # Cargas de bordo
    for I in range(4):
        # Nós iniciais
        if I==0:
            no1 = 1
            no2 = no1+NNx
        elif I==1:
            no1 = NNx
            no2 = no1+NNx
        elif I==2:
            no1 = 1
            no2 = no1+1
        else:
            no1 = NNx*(NNy-1)+1
            no2 = no1+1
            
            if (I==0) or (I==1): # Nós paralelos a y
                for J in range(NEy):
                    Vetor_Carga[6*no1-6] += Cargas_Bordo[I][0]*b*0.5
                    Vetor_Carga[6*no1-5] += Cargas_Bordo[I][1]*b*0.5
                    Vetor_Carga[6*no1-4] += Cargas_Bordo[I][2]*b*0.5
                    Vetor_Carga[6*no1-3] += Cargas_Bordo[I][3]*b*0.5
                    Vetor_Carga[6*no1-2] += Cargas_Bordo[I][4]*b*0.5
                    Vetor_Carga[6*no1-1] += Cargas_Bordo[I][5]*b*0.5
                    
                    Vetor_Carga[6*no2-6] += Cargas_Bordo[I][0]*b*0.5
                    Vetor_Carga[6*no2-5] += Cargas_Bordo[I][1]*b*0.5
                    Vetor_Carga[6*no2-4] += Cargas_Bordo[I][2]*b*0.5
                    Vetor_Carga[6*no2-3] += Cargas_Bordo[I][3]*b*0.5
                    Vetor_Carga[6*no2-2] += Cargas_Bordo[I][4]*b*0.5
                    Vetor_Carga[6*no2-1] += Cargas_Bordo[I][5]*b*0.5
                    
                    no1 = no2
                    no2 = no1+NNx
            elif (I==2) or (I==3): # Nós paralelos a x
                for J in range(NEx):
                    Vetor_Carga[6*no1-6] += Cargas_Bordo[I][0]*a*0.5
                    Vetor_Carga[6*no1-5] += Cargas_Bordo[I][1]*a*0.5
                    Vetor_Carga[6*no1-4] += Cargas_Bordo[I][2]*a*0.5
                    Vetor_Carga[6*no1-3] += Cargas_Bordo[I][3]*a*0.5
                    Vetor_Carga[6*no1-2] += Cargas_Bordo[I][4]*a*0.5
                    Vetor_Carga[6*no1-1] += Cargas_Bordo[I][5]*a*0.5
                    
                    Vetor_Carga[6*no2-6] += Cargas_Bordo[I][0]*a*0.5
                    Vetor_Carga[6*no2-5] += Cargas_Bordo[I][1]*a*0.5
                    Vetor_Carga[6*no2-4] += Cargas_Bordo[I][2]*a*0.5
                    Vetor_Carga[6*no2-3] += Cargas_Bordo[I][3]*a*0.5
                    Vetor_Carga[6*no2-2] += Cargas_Bordo[I][4]*a*0.5
                    Vetor_Carga[6*no2-1] += Cargas_Bordo[I][5]*a*0.5
                    
                    no1 = no2
                    no2 = no1+1
    
    # Cargas de linha paralelas a x
    for I in range(Cargas_Linha_X):
        no1 = Cargas_Linha_X_V[I][0]
        no2 = no1+1
        ql = Cargas_Linha_X_V[I][1]
        
        for J in range(NEx):
            Vetor_Carga[6*no1-4] += ql*a/2 
            Vetor_Carga[6*no1-3] += ql*a**2/12 
            Vetor_Carga[6*no2-4] += ql*a/2 
            Vetor_Carga[6*no2-3] += -ql*a**2/12 
                    
            no1 = no2
            no2 = no1+1        

    # Cargas de linha paralelas a y
    for I in range(Cargas_Linha_Y):
        no1 = Cargas_Linha_Y_V[I][0]
        no2 = no1+NNx
        ql = Cargas_Linha_Y_V[I][1]
        
        for J in range(NEy):
            Vetor_Carga[6*no1-4] += ql*b/2 
            Vetor_Carga[6*no1-2] += ql*b**2/12 
            Vetor_Carga[6*no2-4] += ql*b/2 
            Vetor_Carga[6*no2-2] += -ql*b**2/12 
                    
            no1 = no2
            no2 = no1+NNx        
                    
    # Cargas Concentradas
    if Cargas_Concentradas != 0: # Se houverem cargas concentradas
        for Linha in Cargas_Concentradas_V:
            n_no = Linha[0]
            Vetor_Carga[6*n_no-6] += Linha[1]
            Vetor_Carga[6*n_no-5] += Linha[2]
            Vetor_Carga[6*n_no-4] += Linha[3]
            Vetor_Carga[6*n_no-3] += Linha[4]
            Vetor_Carga[6*n_no-2] += Linha[5]
            Vetor_Carga[6*n_no-1] += Linha[6]
        
def fun_condicoes_contorno():
    # Restrições de bordo
    # Bordo x=0
    Restricoes = Restricoes_Bordo[0]
    if Restricoes[0]==0 or Restricoes[1]==0 or Restricoes[2]==0 or Restricoes[3]==0 or Restricoes[4]==0 or Restricoes[5]==0:
        for I in range(1,NNy+1):
            n_no = (I-1)*NNx+1
            if Restricoes[0] == 0: fun_restringir(6*n_no-6)
            if Restricoes[1] == 0: fun_restringir(6*n_no-5)
            if Restricoes[2] == 0: fun_restringir(6*n_no-4)
            if Restricoes[3] == 0: fun_restringir(6*n_no-3)
            if Restricoes[4] == 0: fun_restringir(6*n_no-2)
            if Restricoes[5] == 0: fun_restringir(6*n_no-1)
    
    # Bordo x=La
    Restricoes = Restricoes_Bordo[1]
    if Restricoes[0]==0 or Restricoes[1]==0 or Restricoes[2]==0 or Restricoes[3]==0 or Restricoes[4]==0 or Restricoes[5]==0:
        for I in range(1,NNy+1):
            n_no = I*NNx
            if Restricoes[0] == 0: fun_restringir(6*n_no-6)
            if Restricoes[1] == 0: fun_restringir(6*n_no-5)
            if Restricoes[2] == 0: fun_restringir(6*n_no-4)
            if Restricoes[3] == 0: fun_restringir(6*n_no-3)
            if Restricoes[4] == 0: fun_restringir(6*n_no-2)
            if Restricoes[5] == 0: fun_restringir(6*n_no-1)
    
    # Bordo y=0
    Restricoes = Restricoes_Bordo[2]
    if Restricoes[0]==0 or Restricoes[1]==0 or Restricoes[2]==0 or Restricoes[3]==0 or Restricoes[4]==0 or Restricoes[5]==0:
        for I in range(1,NNx+1):
            n_no = I
            if Restricoes[0] == 0: fun_restringir(6*n_no-6)
            if Restricoes[1] == 0: fun_restringir(6*n_no-5)
            if Restricoes[2] == 0: fun_restringir(6*n_no-4)
            if Restricoes[3] == 0: fun_restringir(6*n_no-3)
            if Restricoes[4] == 0: fun_restringir(6*n_no-2)
            if Restricoes[5] == 0: fun_restringir(6*n_no-1)
    
    # Bordo y=Lb
    Restricoes = Restricoes_Bordo[3]
    if Restricoes[0]==0 or Restricoes[1]==0 or Restricoes[2]==0 or Restricoes[3]==0 or Restricoes[4]==0 or Restricoes[5]==0:
        for I in range(NN-NNx+1,NN+1):
            n_no = I
            if Restricoes[0] == 0: fun_restringir(6*n_no-6)
            if Restricoes[1] == 0: fun_restringir(6*n_no-5)
            if Restricoes[2] == 0: fun_restringir(6*n_no-4)
            if Restricoes[3] == 0: fun_restringir(6*n_no-3)
            if Restricoes[4] == 0: fun_restringir(6*n_no-2)
            if Restricoes[5] == 0: fun_restringir(6*n_no-1)
    
    # Restrições de canto
    # Canto x=0 y=0
    if Restricoes_Canto[0][0] == 0: fun_restringir(0)
    if Restricoes_Canto[0][1] == 0: fun_restringir(1)
    if Restricoes_Canto[0][2] == 0: fun_restringir(2)
    if Restricoes_Canto[0][3] == 0: fun_restringir(3)
    if Restricoes_Canto[0][4] == 0: fun_restringir(4)
    if Restricoes_Canto[0][5] == 0: fun_restringir(5)
    
    # Canto x=La y=0
    n_no = NNx
    if Restricoes_Canto[1][0] == 0: fun_restringir(6*n_no-6)
    if Restricoes_Canto[1][1] == 0: fun_restringir(6*n_no-5)
    if Restricoes_Canto[1][2] == 0: fun_restringir(6*n_no-4)
    if Restricoes_Canto[1][3] == 0: fun_restringir(6*n_no-3)
    if Restricoes_Canto[1][4] == 0: fun_restringir(6*n_no-2)
    if Restricoes_Canto[1][5] == 0: fun_restringir(6*n_no-1)
    
    # Canto x=0 y=Lb
    n_no = NN-NNx+1
    if Restricoes_Canto[2][0] == 0: fun_restringir(6*n_no-6)
    if Restricoes_Canto[2][1] == 0: fun_restringir(6*n_no-5)
    if Restricoes_Canto[2][2] == 0: fun_restringir(6*n_no-4)
    if Restricoes_Canto[2][3] == 0: fun_restringir(6*n_no-3)
    if Restricoes_Canto[2][4] == 0: fun_restringir(6*n_no-2)
    if Restricoes_Canto[2][5] == 0: fun_restringir(6*n_no-1)
    
    # Canto x=0 y=Lb
    n_no = NN
    if Restricoes_Canto[3][0] == 0: fun_restringir(6*n_no-6)
    if Restricoes_Canto[3][1] == 0: fun_restringir(6*n_no-5)
    if Restricoes_Canto[3][2] == 0: fun_restringir(6*n_no-4)
    if Restricoes_Canto[3][3] == 0: fun_restringir(6*n_no-3)
    if Restricoes_Canto[3][4] == 0: fun_restringir(6*n_no-2)
    if Restricoes_Canto[3][5] == 0: fun_restringir(6*n_no-1)
    
    # Restricoes em nós específicos
    for I in range(Restricoes_Nos):
        n_no = Restricoes_Nos_V[I][0]
        if Restricoes_Nos_V[I][1] == 0: fun_restringir(6*n_no-6)
        if Restricoes_Nos_V[I][2] == 0: fun_restringir(6*n_no-5)
        if Restricoes_Nos_V[I][3] == 0: fun_restringir(6*n_no-4)
        if Restricoes_Nos_V[I][4] == 0: fun_restringir(6*n_no-3)
        if Restricoes_Nos_V[I][5] == 0: fun_restringir(6*n_no-2)
        if Restricoes_Nos_V[I][6] == 0: fun_restringir(6*n_no-1)
    
    
def fun_restringir(Grau):
    # O programa tá iniciando com índice 0 nas matrizes
    
    # Linhas
    for I in range(2*Banda-1):
        Matriz[I][Grau]= 0.0
    
    # Colunas
    for J in range(NGL):
        I = Grau-J+Banda-1
        if (I>=0) and (I<2*Banda-1):
            Matriz[I][J] = 0.0
    
    # Diagonal principal
    Matriz[Banda-1][Grau] = 1.0
    Vetor_Carga[Grau] = 0.0
    
    
def fun_resolucao_sistema():
    global Desl
    
    Desl = []
    for I in range(NGL): Desl.append(0.0)
    
    # Eliminação de Gauss
    for I in range(NGL-1):
        if Matriz[I][I] != 0:
            PIVO = Matriz[I][I]
            for K in range(I+1,I+Banda+1): # Varrer as linhas
                if (K>=NGL): break # As linhas vão até NGL-1, o índice começa em 0
                AUX = -Matriz[K][I]/PIVO
                for J in range(I,I+Banda+1):
                    if (J>=NGL): break     # As colunas vão até NGL-1, o índice começa em 0
                    Matriz[K][J] += AUX*Matriz[I][J]
                # Vetor de forças
                Vetor_Carga[K] += AUX*Vetor_Carga[I]
        else:
            print ("Pivô nulo")
            break
            
    
    # Retrosubstituição
    for K in range(NGL-1,-1,-1):
        soma = Vetor_Carga[K]
        for J in range(NGL-1,K,-1):
            soma = soma - Matriz[K][J]*Desl[J]
        Desl[K] = soma/Matriz[K][K]

def fun_calcular_derivadas():
    global DeriG
    global DeriGE # Derivadas no nó central
    
    #Inicializar as listas
    VetorL = []
    for I in range(24): VetorL.append(0)
    
    DER = []
    for I in range(44): DER.append(0)
    
    DERI = []
    DERIL = []
    for I in range(4*NN):
        for J in range(12):
            DERIL.append(0)
        DERI.append(DERIL)
        DERIL = []
    
    DeriG = []
    DeriGL = []
    for I in range(NN):
        for J in range(12):
            DeriGL.append(0)
        DeriG.append(DeriGL)
        DeriGL = []
    
    DeriGE = []
    DeriGEL = []
    for I in range(NE):
        for J in range(12):
            DeriGEL.append(0)
        DeriGE.append(DeriGEL)
        DeriGEL = []
        
    MEDIA = []
    for I in range(12): MEDIA.append(0)
    
    
    # Registrar a linha da matriz em que estão armazenadas as derivadas no nó    
    cont = 0
    
    # Calcular as derivadas nos nós da malha
    for I in range(NE): # passar por todos os elementos
        
        # Formar o vetor de deslocamentos do elemento
        for J in range(4): # 4 nós por elemento
            for K in range(6): # 6 graus de liberdade por nó
                VetorL[6*J+K] = Desl[6*Incidencia[I][J]-6+K]

        DER[0]=-6/a**2*VetorL[2]-4/a*VetorL[3]+6/a**2*VetorL[8]-2/a*VetorL[9]
        DER[1]=1*VetorL[5]
        DER[2]=-6/b**2*VetorL[2]-4/b*VetorL[4]+6/b**2*VetorL[14]-2/b*VetorL[16]
        DER[3]=12/a**3*VetorL[2]+6/a**2*VetorL[3]-12/a**3*VetorL[8]+6/a**2*VetorL[9]
        DER[4]=-6/a**2*VetorL[4]-4/a*VetorL[5]+6/a**2*VetorL[10]-2/a*VetorL[11]
        DER[5]=-6/b**2*VetorL[3]-4/b*VetorL[5]+6/b**2*VetorL[15]-2/b*VetorL[17]
        DER[6]=12/b**3*VetorL[2]+6/b**2*VetorL[4]-12/b**3*VetorL[14]+6/b**2*VetorL[16]
        DER[7]=-1/a*VetorL[0]+1/a*VetorL[6]
        DER[8]=-1/b*VetorL[0]+1/b*VetorL[12]
        DER[9]=-1/a*VetorL[1]+1/a*VetorL[7]
        DER[10]=-1/b*VetorL[1]+1/b*VetorL[13]
        DER[11]=6/a**2*VetorL[2]+2/a*VetorL[3]-6/a**2*VetorL[8]+4/a*VetorL[9]
        DER[12]=1*VetorL[11]
        DER[13]=-6/b**2*VetorL[8]-4/b*VetorL[10]+6/b**2*VetorL[20]-2/b*VetorL[22]
        DER[14]=12/a**3*VetorL[2]+6/a**2*VetorL[3]-12/a**3*VetorL[8]+6/a**2*VetorL[9]
        DER[15]=6/a**2*VetorL[4]+2/a*VetorL[5]-6/a**2*VetorL[10]+4/a*VetorL[11]
        DER[16]=-6/b**2*VetorL[9]-4/b*VetorL[11]+6/b**2*VetorL[21]-2/b*VetorL[23]
        DER[17]=12/b**3*VetorL[8]+6/b**2*VetorL[10]-12/b**3*VetorL[20]+6/b**2*VetorL[22]
        DER[18]=-1/a*VetorL[0]+1/a*VetorL[6]
        DER[19]=-1/b*VetorL[6]+1/b*VetorL[18]
        DER[20]=-1/a*VetorL[1]+1/a*VetorL[7]
        DER[21]=-1/b*VetorL[7]+1/b*VetorL[19]
        DER[22]=-6/a**2*VetorL[14]-4/a*VetorL[15]+6/a**2*VetorL[20]-2/a*VetorL[21]
        DER[23]=1*VetorL[17]
        DER[24]=6/b**2*VetorL[2]+2/b*VetorL[4]-6/b**2*VetorL[14]+4/b*VetorL[16]
        DER[25]=12/a**3*VetorL[14]+6/a**2*VetorL[15]-12/a**3*VetorL[20]+6/a**2*VetorL[21]
        DER[26]=-6/a**2*VetorL[16]-4/a*VetorL[17]+6/a**2*VetorL[22]-2/a*VetorL[23]
        DER[27]=6/b**2*VetorL[3]+2/b*VetorL[5]-6/b**2*VetorL[15]+4/b*VetorL[17]
        DER[28]=12/b**3*VetorL[2]+6/b**2*VetorL[4]-12/b**3*VetorL[14]+6/b**2*VetorL[16]
        DER[29]=-1/a*VetorL[12]+1/a*VetorL[18]
        DER[30]=-1/b*VetorL[0]+1/b*VetorL[12]
        DER[31]=-1/a*VetorL[13]+1/a*VetorL[19]
        DER[32]=-1/b*VetorL[1]+1/b*VetorL[13]
        DER[33]=6/a**2*VetorL[14]+2/a*VetorL[15]-6/a**2*VetorL[20]+4/a*VetorL[21]
        DER[34]=1*VetorL[23]
        DER[35]=6/b**2*VetorL[8]+2/b*VetorL[10]-6/b**2*VetorL[20]+4/b*VetorL[22]
        DER[36]=12/a**3*VetorL[14]+6/a**2*VetorL[15]-12/a**3*VetorL[20]+6/a**2*VetorL[21]
        DER[37]=6/a**2*VetorL[16]+2/a*VetorL[17]-6/a**2*VetorL[22]+4/a*VetorL[23]
        DER[38]=6/b**2*VetorL[9]+2/b*VetorL[11]-6/b**2*VetorL[21]+4/b*VetorL[23]
        DER[39]=12/b**3*VetorL[8]+6/b**2*VetorL[10]-12/b**3*VetorL[20]+6/b**2*VetorL[22]
        DER[40]=-1/a*VetorL[12]+1/a*VetorL[18]
        DER[41]=-1/b*VetorL[6]+1/b*VetorL[18]
        DER[42]=-1/a*VetorL[13]+1/a*VetorL[19]
        DER[43]=-1/b*VetorL[7]+1/b*VetorL[19]
        
        for J in range(4):
            DERI[cont][0] = Incidencia[I][J] # número do nó
            for cont2 in range(1,12):
                DERI[cont][cont2] = DER[11*J+cont2-1] # não precisa J-1, começa a contar em 0
            cont += 1 # próxima linha                                # -1 porque o índice começa em 0
                
        
    # Médias nodais
    for I in range(1,NN+1): # Passar por todos os números dos nós
        denominador = 0
        MEDIA[0] = I #número do nó
        for cont in range(1,12): MEDIA[cont] = 0
        
        # Procurar os valores em DERI para o mesmo nó
        for J in range(4*NE):
            #print DERI[J][0],I
            if DERI[J][0] == I: 
                denominador += 1 # Guardar o número do nó na primeira linha
                for cont in range(1,12):
                    MEDIA[cont] += DERI[J][cont]
        
        # Calcular as médias de fato
        for J in range(1,12):
            MEDIA[J] = MEDIA[J]/denominador
        
        # Armazenar as derivadas em DeriG
        for J in range(12):
            DeriG[I-1][J] = MEDIA[J]
    
    # Calcular as derivadas no centro dos elementos
    for I in range(NE):
        
        # Formar o vetor de deslocamentos do elemento
        for J in range(4): # 4 nós por elemento
            for K in range(6): # 6 graus de liberdade por nó
                VetorL[6*J+K] = Desl[6*Incidencia[I][J]-6+K]
        
        DER[0]=-1/(2*a)*VetorL[3]-b/(8*a)*VetorL[5]+1/(2*a)*VetorL[9]+b/(8*a)*VetorL[11]-1/(2*a)*VetorL[15]+b/(8*a)*VetorL[17]+1/(2*a)*VetorL[21]-b/(8*a)*VetorL[23]
        DER[1]=+9/(4*a*b)*VetorL[2]+3/(8*b)*VetorL[3]+3/(8*a)*VetorL[4]+1/16*VetorL[5]-9/(4*a*b)*VetorL[8]+3/(8*b)*VetorL[9]-3/(8*a)*VetorL[10]+1/16*VetorL[11]-9/(4*a*b)*VetorL[14]-3/(8*b)*VetorL[15]+3/(8*a)*VetorL[16]+1/16*VetorL[17]+9/(4*a*b)*VetorL[20]-3/(8*b)*VetorL[21]-3/(8*a)*VetorL[22]+1/16*VetorL[23]
        DER[2]=-1/(2*b)*VetorL[4]-a/(8*b)*VetorL[5]-1/(2*b)*VetorL[10]+a/(8*b)*VetorL[11]+1/(2*b)*VetorL[16]+a/(8*b)*VetorL[17]+1/(2*b)*VetorL[22]-a/(8*b)*VetorL[23]
        DER[3]=+6/a**3*VetorL[2]+3/a**2*VetorL[3]+3*b/(2*a**3)*VetorL[4]+3*b/(4*a**2)*VetorL[5]-6/a**3*VetorL[8]+3/a**2*VetorL[9]-3*b/(2*a**3)*VetorL[10]+3*b/(4*a**2)*VetorL[11]+6/a**3*VetorL[14]+3/a**2*VetorL[15]-3*b/(2*a**3)*VetorL[16]-3*b/(4*a**2)*VetorL[17]-6/a**3*VetorL[20]+3/a**2*VetorL[21]+3*b/(2*a**3)*VetorL[22]-3*b/(4*a**2)*VetorL[23]
        DER[4]=+3/(2*a*b)*VetorL[3]+1/(4*a)*VetorL[5]-3/(2*a*b)*VetorL[9]-1/(4*a)*VetorL[11]-3/(2*a*b)*VetorL[15]+1/(4*a)*VetorL[17]+3/(2*a*b)*VetorL[21]-1/(4*a)*VetorL[23]
        DER[5]=+3/(2*a*b)*VetorL[4]+1/(4*b)*VetorL[5]-3/(2*a*b)*VetorL[10]+1/(4*b)*VetorL[11]-3/(2*a*b)*VetorL[16]-1/(4*b)*VetorL[17]+3/(2*a*b)*VetorL[22]-1/(4*b)*VetorL[23]
        DER[6]=+6/b**3*VetorL[2]+3*a/(2*b**3)*VetorL[3]+3/b**2*VetorL[4]+3*a/(4*b**2)*VetorL[5]+6/b**3*VetorL[8]-3*a/(2*b**3)*VetorL[9]+3/b**2*VetorL[10]-3*a/(4*b**2)*VetorL[11]-6/b**3*VetorL[14]-3*a/(2*b**3)*VetorL[15]+3/b**2*VetorL[16]+3*a/(4*b**2)*VetorL[17]-6/b**3*VetorL[20]+3*a/(2*b**3)*VetorL[21]+3/b**2*VetorL[22]-3*a/(4*b**2)*VetorL[23]
        DER[7]=-1/(2*a)*VetorL[0]+1/(2*a)*VetorL[6]-1/(2*a)*VetorL[12]+1/(2*a)*VetorL[18]
        DER[8]=-1/(2*b)*VetorL[0]-1/(2*b)*VetorL[6]+1/(2*b)*VetorL[12]+1/(2*b)*VetorL[18]
        DER[9]=-1/(2*a)*VetorL[1]+1/(2*a)*VetorL[7]-1/(2*a)*VetorL[13]+1/(2*a)*VetorL[19]
        DER[10]=-1/(2*b)*VetorL[1]-1/(2*b)*VetorL[7]+1/(2*b)*VetorL[13]+1/(2*b)*VetorL[19]
        
        DeriGE[I][0] = I+1 # número do elemento
        for cont2 in range(11):
            DeriGE[I][cont2+1] = DER[cont2] 
        #cont += 1 # próxima linha                            

def fun_calcular_deformacoes_tensoes():
    global Deformacao
    global Tensao
    global DeformacaoE
    global TensaoE
    
    # Inicializar as listas
    Deformacao = []
    DeformacaoM = []
    DeformacaoL = []
    for I in range(Num_Laminas):
        for J in range(NN):
            for K in range(7):
                DeformacaoL.append(0)
            DeformacaoM.append(DeformacaoL)
            DeformacaoL = []
        Deformacao.append(DeformacaoM)
        DeformacaoM = []
    
    Tensao = []
    TensaoM = []
    TensaoL = []
    for I in range(Num_Laminas):
        for J in range(NN):
            for K in range(7):
                TensaoL.append(0)
            TensaoM.append(TensaoL)
            TensaoL = []
        Tensao.append(TensaoM)
        TensaoM = []

    DeformacaoE = []
    DeformacaoEM = []
    DeformacaoEL = []
    for I in range(Num_Laminas):
        for J in range(NE):
            for K in range(4):
                DeformacaoEL.append(0)
            DeformacaoEM.append(DeformacaoEL)
            DeformacaoEL = []
        DeformacaoE.append(DeformacaoEM)
        DeformacaoEM = []
    
    TensaoE = []
    TensaoEM = []
    TensaoEL = []

    for I in range(Num_Laminas):
        for J in range(NE):
            for K in range(4):
                TensaoEL.append(0)
            TensaoEM.append(TensaoEL)
            TensaoEL = []
        TensaoE.append(TensaoEM)
        TensaoEM = []    
    
    for I in range(Num_Laminas):
        Q11_R = Prop_Lam[I][0]
        Q12_R = Prop_Lam[I][1]
        Q16_R = Prop_Lam[I][2]
        Q22_R = Prop_Lam[I][3]
        Q26_R = Prop_Lam[I][4]
        Q66_R = Prop_Lam[I][5]
        
        for J in range(NN):
            #CÁLCULO DAS DEFORMAÇÕES
            Ex1 = DeriG[J][8]-Lam_C[I][6]*DeriG[J][1]
            Ex2 = DeriG[J][8]-Lam_C[I][7]*DeriG[J][1]
            Ey1 = DeriG[J][11]-Lam_C[I][6]*DeriG[J][3]
            Ey2 = DeriG[J][11]-Lam_C[I][7]*DeriG[J][3]
            Exy1 = DeriG[J][9]+DeriG[J][10]-2*Lam_C[I][6]*DeriG[J][2]
            Exy2 = DeriG[J][9]+DeriG[J][10]-2*Lam_C[I][7]*DeriG[J][2]
    
            #ESCREVER AS DEFORMACOES EM UMA MATRIZ
            # I = número da lâmina, J = número do nó 
            Deformacao[I][J][0] = J+1
            Deformacao[I][J][1] = Ex1
            Deformacao[I][J][2] = Ex2
            Deformacao[I][J][3] = Ey1
            Deformacao[I][J][4] = Ey2
            Deformacao[I][J][5] = Exy1
            Deformacao[I][J][6] = Exy2
            
            Tensao[I][J][0] = J+1
            Tensao[I][J][1] = Q11_R*Ex1+Q12_R*Ey1+Q16_R*Exy1
            Tensao[I][J][2] = Q11_R*Ex2+Q12_R*Ey2+Q16_R*Exy2
            Tensao[I][J][3] = Q12_R*Ex1+Q22_R*Ey1+Q16_R*Exy1
            Tensao[I][J][4] = Q12_R*Ex2+Q22_R*Ey2+Q16_R*Exy2
            Tensao[I][J][5] = Q16_R*Ex1+Q26_R*Ey1+Q66_R*Exy1
            Tensao[I][J][6] = Q16_R*Ex2+Q26_R*Ey2+Q66_R*Exy2        
            
    # Deformações e tensões no centro dos elementos        
    for I in range(Num_Laminas):
        Q11_R = Prop_Lam[I][0]
        Q12_R = Prop_Lam[I][1]
        Q16_R = Prop_Lam[I][2]
        Q22_R = Prop_Lam[I][3]
        Q26_R = Prop_Lam[I][4]
        Q66_R = Prop_Lam[I][5]
        
        for J in range(NE):
            #CÁLCULO DAS DEFORMAÇÕES
            Ex = DeriGE[J][8]-Lam_C[I][8]*DeriGE[J][1] # Lam_C[I][8] = hmed
            Ey = DeriGE[J][11]-Lam_C[I][8]*DeriGE[J][3]
            Exy = DeriGE[J][9]+DeriGE[J][10]-2*Lam_C[I][8]*DeriGE[J][2]
    
            #ESCREVER AS DEFORMACOES EM UMA MATRIZ
            # I = número da lâmina, J = número do nó 
            DeformacaoE[I][J][0] = J+1
            DeformacaoE[I][J][1] = Ex
            DeformacaoE[I][J][2] = Ey
            DeformacaoE[I][J][3] = Exy

            
            TensaoE[I][J][0] = J+1
            TensaoE[I][J][1] = Q11_R*Ex+Q12_R*Ey+Q16_R*Exy
            TensaoE[I][J][2] = Q12_R*Ex+Q22_R*Ey+Q26_R*Exy
            TensaoE[I][J][3] = Q16_R*Ex+Q26_R*Ey+Q66_R*Exy
            

def fun_calcular_esforcos():
    global ESFORCOS
    
    A11,A12,A16,A22,A26,A66 = A[0],A[1],A[2],A[3],A[4],A[5]
    B11,B12,B16,B22,B26,B66 = B[0],B[1],B[2],B[3],B[4],B[5]
    D11,D12,D16,D22,D26,D66 = D[0],D[1],D[2],D[3],D[4],D[5]
    
    # Inicializar as listas
    Esf = []
    for I in range(7): Esf.append(0)
    
    ESFORCOS = []
    ESFORCOSL = []
    for I in range(NN):
        for J in range(7):
            ESFORCOSL.append(0)
        ESFORCOS.append(ESFORCOSL)
        ESFORCOSL = []
    
    for I in range(NN):
        Esf[0] = I+1 # número do nó
        Esf[1] = B11*DeriG[I][8]+B12*DeriG[I][11]+B16*(DeriG[I][9]+DeriG[I][10])-D11*DeriG[I][1]-D12*DeriG[I][3]-2*D16*DeriG[I][2]
        Esf[2] = B12*DeriG[I][8]+B22*DeriG[I][11]+B26*(DeriG[I][9]+DeriG[I][10])-D12*DeriG[I][1]-D22*DeriG[I][3]-2*D26*DeriG[I][2]
        Esf[3] = B16*DeriG[I][8]+B26*DeriG[I][11]+B66*(DeriG[I][9]+DeriG[I][10])-D16*DeriG[I][1]-D26*DeriG[I][3]-2*D66*DeriG[I][2]
        Esf[4]=  A11*DeriG[I][8]+A12*DeriG[I][11]+A16*(DeriG[I][9]+DeriG[I][10])-B11*DeriG[I][1]-B12*DeriG[I][3]-2*B16*DeriG[I][2]
        Esf[5]=  A12*DeriG[I][8]+A22*DeriG[I][11]+A26*(DeriG[I][9]+DeriG[I][10])-B12*DeriG[I][1]-B22*DeriG[I][3]-2*B26*DeriG[I][2]
        Esf[6]=  A16*DeriG[I][8]+A26*DeriG[I][11]+A66*(DeriG[I][9]+DeriG[I][10])-B16*DeriG[I][1]-B26*DeriG[I][3]-2*B66*DeriG[I][2]
        
        for J in range(7):
            ESFORCOS[I][J] = Esf[J]

def fun_escrever_resultados():
    Arquivo_Saida.write("Dimensões da placa: %f, %f \n" %(La,Lb))
    Arquivo_Saida.write("Número de elementos da placa: %d, %d \n" %(NEx,NEy))
    Arquivo_Saida.write("Total de elementos: %d \n" %(NE))
    Arquivo_Saida.write("Número de nós da placa: %d, %d \n" %(NNx,NNy))
    Arquivo_Saida.write("Total de elementos: %d \n" %(NN))
    Arquivo_Saida.write("Número de nós monitorados: %d \n" %(Num_Nos_Monitorados))
    
    Arquivo_Saida.write("Nós monitorados: ")
    for I in range(Num_Nos_Monitorados): 
        Arquivo_Saida.write("%d " %(Nos_Monitorados[I])) 
    else: Arquivo_Saida.write("\n")
    
    Arquivo_Saida.write("\nNúmero de materiais: %d\n" %(Num_Materiais))
    Arquivo_Saida.write("Propriedades dos materiais\n")
    
    Arquivo_Saida.write("N   Material      E1           E2          G12      v12\n")
    for I in range(Num_Materiais):
        Arquivo_Saida.write("%d %10s %6.6e %6.6e %6.6e %.2f\n" %(Materiais[I][0],Materiais[I][1],Materiais[I][2],Materiais[I][3],Materiais[I][4],Materiais[I][5]))
    
    Arquivo_Saida.write("\nNúmero de lâminas: %d\n" %(Num_Laminas))
    Arquivo_Saida.write("Propriedades das lâminas\n")
    Arquivo_Saida.write("Material Espessura Ângulo\n")
    for I in range(Num_Laminas):
        Arquivo_Saida.write("%4d %12.6f  %6.2f\n" %(Laminas[I][0],Laminas[I][1],Laminas[I][2]))
    
    Arquivo_Saida.write("\nPropriedades completas da lâmina\n")
    Arquivo_Saida.write("   N     E1            E2          G12        v12    v21   Ângulo    hi        hf        hm\n")
    for I in range(Num_Laminas):
        Arquivo_Saida.write("%4d %12.6e %12.6e %12.6e %6.2f %6.2f %6.2f %9.6f %9.6f %9.6f\n" 
                            %(I+1,Lam_C[I][0],Lam_C[I][1],Lam_C[I][2],Lam_C[I][3],Lam_C[I][4],Lam_C[I][5],Lam_C[I][6],Lam_C[I][7],Lam_C[I][8]))    
    
    if Carga_Distribuida == 1:
        Arquivo_Saida.write("\nHá carga distribuída.\n")
        Arquivo_Saida.write("q: %6.6e\n" %(Carga_Distribuida_V))
    else:
        Arquivo_Saida.write("\nNâo há carga distribuída.\n")
    
    if Carga_Trapezoidal == 1:
        Arquivo_Saida.write("\nHá carga trapezoidal.")
        Arquivo_Saida.write("\n     q0           q1\n")
        Arquivo_Saida.write("%6.6e %6.6e\n" %(Carga_Trapezoidal_V[0],Carga_Trapezoidal_V[1]))
    else:
        Arquivo_Saida.write("\nNâo há carga trapezoidal.\n")
    
    Arquivo_Saida.write("\nCargas de bordo\n")
    Arquivo_Saida.write("     Fx           Fy           Fz            Mx           My          Mxy\n")
    for I in range(4):
        Arquivo_Saida.write("%6.6e %6.6e %6.6e %6.6e %6.6e %6.6e\n" %(Cargas_Bordo[I][0],Cargas_Bordo[I][1],Cargas_Bordo[I][2],Cargas_Bordo[I][3],Cargas_Bordo[I][4],Cargas_Bordo[I][5]))
    
    if Cargas_Linha_X == 0:
        Arquivo_Saida.write("\nNão há carga de linha paralela a x.\n")
    else:
        Arquivo_Saida.write("\nHá %d carga(s) de linha paralela(s) a x.\n" %(Cargas_Linha_X))
        Arquivo_Saida.write("    No            q\n")
        for I in range(Cargas_Linha_X):
            Arquivo_Saida.write("%4d %6.6e\n" %(Cargas_Linha_X_V[I][0],Cargas_Linha_X_V[I][1]))
    
    if Cargas_Linha_Y == 0:
        Arquivo_Saida.write("\nNão há carga de linha paralela a y.\n")
    else:
        Arquivo_Saida.write("\nHá %d carga(s) de linha paralela(s) a y.\n" %(Cargas_Linha_Y))
        Arquivo_Saida.write("    No            q\n")
        for I in range(Cargas_Linha_Y):
            Arquivo_Saida.write("%4d %6.6e\n" %(Cargas_Linha_Y_V[I][0],Cargas_Linha_Y_V[I][1]))
    
    Arquivo_Saida.write("\nRestrições de bordo - 0-Restrito 1-Livre\n")
    Arquivo_Saida.write("u      v      w    ∂w/∂x  ∂w/∂y ∂²w/∂x∂y\n")
    for I in range(4):
        Arquivo_Saida.write("%d      %d      %d      %d      %d      %d\n" %(Restricoes_Bordo[I][0],Restricoes_Bordo[I][1],
                               Restricoes_Bordo[I][2],Restricoes_Bordo[I][3],Restricoes_Bordo[I][4],Restricoes_Bordo[I][5]))
        
    
    Arquivo_Saida.write("\nRestrições de canto - 0-Restrito 1-Livre\n")
    Arquivo_Saida.write("u      v      w    ∂w/∂x  ∂w/∂y ∂²w/∂x∂y\n")
    for I in range(4):
        Arquivo_Saida.write("%d      %d      %d      %d      %d      %d\n" %(Restricoes_Canto[I][0],Restricoes_Canto[I][1],
                               Restricoes_Canto[I][2],Restricoes_Canto[I][3],Restricoes_Canto[I][4],Restricoes_Canto[I][5]))
    
    if Restricoes_Nos == 0:
        Arquivo_Saida.write("\nNão há nós específicos restritos.\n")
    else:
        Arquivo_Saida.write("\nHá %d nó(s) específico(s) restrito(s).\n" %(Restricoes_Nos))
        Arquivo_Saida.write("  No      u      v      w    ∂w/∂x  ∂w/∂y ∂²w/∂x∂y\n")
        for I in range(Restricoes_Nos):
            Arquivo_Saida.write("%4d      %d      %d      %d      %d      %d      %d\n" %(Restricoes_Nos_V[I][0],Restricoes_Nos_V[I][1],
                               Restricoes_Nos_V[I][2],Restricoes_Nos_V[I][3],Restricoes_Nos_V[I][4],Restricoes_Nos_V[I][5],Restricoes_Nos_V[I][6]))

    # Deslocamentos
    Arquivo_Saida.write("\n                                                   DESLOCAMENTOS\n")
    Arquivo_Saida.write("  Nó          u                v                 w             ∂w/∂x            ∂w/∂y           ∂²w/∂x∂y\n")
    for I in range(1,NN+1):
        Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n" %(I,Desl[6*I-6],Desl[6*I-5],Desl[6*I-4],Desl[6*I-3],Desl[6*I-2],Desl[6*I-1]))

    # Derivadas
    Arquivo_Saida.write("\n                                                                                        DERIVADAS\n")
    Arquivo_Saida.write("  Nó        ∂²w/∂x²         ∂²w/∂x∂y          ∂²w/∂y²          ∂²w/∂x³        ∂²w/∂x²∂y         ∂²w/∂x∂y²         ∂²w/∂y³          ∂u/∂x            ∂u/∂y            ∂v/∂x             ∂v/∂y\n")
    for I in range(NN):
        Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n" 
                                            %(DeriG[I][0],DeriG[I][1],DeriG[I][2],DeriG[I][3],DeriG[I][4],DeriG[I][5],DeriG[I][6],DeriG[I][7],
                                            DeriG[I][8],DeriG[I][9],DeriG[I][10],DeriG[I][11]))
    
    # Esforços
    Arquivo_Saida.write("\n                                                      ESFORÇOS\n")
    Arquivo_Saida.write("  Nó          Mxx             Myy              Mxy              Nxx              Nyy              Nxy\n")
    for I in range(NN):
        Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n" %(ESFORCOS[I][0],ESFORCOS[I][1],
                                            ESFORCOS[I][2],ESFORCOS[I][3],ESFORCOS[I][4],ESFORCOS[I][5],ESFORCOS[I][6]))
    
    # Deformações
    for I in range(Num_Laminas):
        Arquivo_Saida.write("\n                                                      LÂMINA %d\n" %(I+1))
        Arquivo_Saida.write("                                                    DEFORMAÇÕES\n")
        Arquivo_Saida.write("  Nó       εxx_sup          εxx_inf          εyy_sup          εyy_inf          γxy_sup          γxy_inf\n")
        for J in range(NN):
            Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                                %(Deformacao[I][J][0],Deformacao[I][J][1],Deformacao[I][J][2],Deformacao[I][J][3],Deformacao[I][J][4],Deformacao[I][J][5],Deformacao[I][J][6]))
    # Tensões          
        Arquivo_Saida.write("\n                                                      TENSÕES\n")
        Arquivo_Saida.write("  Nó       σxx_sup          σxx_inf          σyy_sup          σyy_inf          τxy_sup          τxy_inf\n")
        for J in range(NN):
            Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                                %(Tensao[I][J][0],Tensao[I][J][1],Tensao[I][J][2],Tensao[I][J][3],Tensao[I][J][4],Tensao[I][J][5],Tensao[I][J][6]))
   
    # Deformações e tensões nos elementos   
    for I in range(Num_Laminas):
        Arquivo_Saida.write("\n                         LÂMINA %d\n" %(I+1))
    # Deformações        
        Arquivo_Saida.write("                DEFORMAÇÕES NOS ELEMENTOS\n")
        Arquivo_Saida.write("  Elem       εxx              εyy              εxy\n")
        for J in range(NE):
            Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e \n"
                                %(DeformacaoE[I][J][0],DeformacaoE[I][J][1],DeformacaoE[I][J][2],DeformacaoE[I][J][3]))
    # Tensões          
        Arquivo_Saida.write("\n                TENSÕES NOS ELEMENTOS\n")
        Arquivo_Saida.write("  Elem       σxx              σyy              σxy\n")
        for J in range(NE):
            Arquivo_Saida.write("%4d %16.6e %16.6e %16.6e\n"
                                %(TensaoE[I][J][0],TensaoE[I][J][1],TensaoE[I][J][2],TensaoE[I][J][3]))

        
    # Saída dos valores para os nós monitorados
    Arquivo_Monitorados = open(nome_job + "_monitorados.txt","w")
    
    # Número de nós monitorados
    Arquivo_Monitorados.write("Número de nós monitorados: %d \n" %(Num_Nos_Monitorados))
    
   
    if Num_Nos_Monitorados != 0:
        Arquivo_Monitorados.write("Nós monitorados: ")
        for I in range(Num_Nos_Monitorados): 
            Arquivo_Monitorados.write("%d " %(Nos_Monitorados[I])) 
        else: Arquivo_Monitorados.write("\n")
        
        # Deslocamentos
        Arquivo_Monitorados.write("\n                                                   DESLOCAMENTOS\n")
        Arquivo_Monitorados.write("  Nó          u                v                 w             ∂w/∂x            ∂w/∂y           ∂²w/∂x∂y\n")
        
        for I in Nos_Monitorados:
            Arquivo_Monitorados.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n" %(I,Desl[6*I-6],Desl[6*I-5],Desl[6*I-4],Desl[6*I-3],Desl[6*I-2],Desl[6*I-1])) 
        
        # Derivadas
        Arquivo_Monitorados.write("\n                                                                                        DERIVADAS\n")
        Arquivo_Monitorados.write("  Nó        ∂²w/∂x²         ∂²w/∂x∂y          ∂²w/∂y²          ∂²w/∂x³        ∂²w/∂x²∂y         ∂²w/∂x∂y²         ∂²w/∂y³          ∂u/∂x            ∂u/∂y            ∂v/∂x             ∂v/∂y\n")
        
        for I in range(Num_Nos_Monitorados):
            J = Nos_Monitorados[I]-1 # corresponder à linha
            Arquivo_Monitorados.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n" 
                                            %(DeriG[J][0],DeriG[J][1],DeriG[J][2],DeriG[J][3],DeriG[J][4],DeriG[J][5],DeriG[J][6],DeriG[J][7],
                                            DeriG[J][8],DeriG[J][9],DeriG[J][10],DeriG[J][11]))

        # Esforços
        Arquivo_Monitorados.write("\n                                                      ESFORÇOS\n")
        Arquivo_Monitorados.write("  Nó          Mxx             Myy              Mxy              Nxx              Nyy              Nxy\n")
        for I in range(Num_Nos_Monitorados):
            J = Nos_Monitorados[I]-1 # corresponder à linha
            Arquivo_Monitorados.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n" %(ESFORCOS[J][0],ESFORCOS[J][1],
                                            ESFORCOS[J][2],ESFORCOS[J][3],ESFORCOS[J][4],ESFORCOS[J][5],ESFORCOS[J][6]))
  
        # Deformações e Tensões
        for I in range(Num_Laminas):
            Arquivo_Monitorados.write("\n                                                      LÂMINA %d\n" %(I+1))
            Arquivo_Monitorados.write("                                                    DEFORMAÇÕES\n")
            Arquivo_Monitorados.write("  Nó       εxx_sup          εxx_inf          εyy_sup          εyy_inf          γxy_sup          γxy_inf\n")
            for J in range(Num_Nos_Monitorados):
                K = Nos_Monitorados[J]-1 # corresponder à linha
                Arquivo_Monitorados.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                                %(Deformacao[I][K][0],Deformacao[I][K][1],Deformacao[I][K][2],Deformacao[I][K][3],Deformacao[I][K][4],Deformacao[I][K][5],Deformacao[I][K][6]))
                
            Arquivo_Monitorados.write("\n                                                      TENSÕES\n")
            Arquivo_Monitorados.write("  Nó       σxx_sup          σxx_inf          σyy_sup          σyy_inf          τxy_sup          τxy_inf\n")
            for J in range(Num_Nos_Monitorados):
                K = Nos_Monitorados[J]-1
                Arquivo_Monitorados.write("%4d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                                %(Tensao[I][K][0],Tensao[I][K][1],Tensao[I][K][2],Tensao[I][K][3],Tensao[I][K][4],Tensao[I][K][5],Tensao[I][K][6]))
   
   # Fechar o arquivo de saída e dos nós monitorados
    Arquivo_Saida.close()
    Arquivo_Monitorados.close()
    
				
    # Saida de dados para o visualizador
    Arquivo_Job = open(nome_job + ".job", "w")
    Arquivo_Coord = open(nome_job + "_coord.txt", "w")
    Arquivo_Incidencia = open(nome_job + "_incidencia.txt", "w")
    Arquivo_Monitorados = open(nome_job + "_vmonitorados.txt", "w")
    Arquivo_Desl = open(nome_job + "_desl.txt", "w")
    Arquivo_Esf = open(nome_job + "_esf.txt", "w")
    Arquivo_Deform = open(nome_job + "_deform.txt", "w")
    Arquivo_Tens = open(nome_job + "_tens.txt", "w")
    
    # Arquivo Job
    Arquivo_Job.write("%.6f\n" %La)
    Arquivo_Job.write("%.6f\n" %Lb)
    Arquivo_Job.write("%d\n" %NEx)
    Arquivo_Job.write("%d\n" %NEy)
    Arquivo_Job.write("%d\n" %Num_Nos_Monitorados)
    Arquivo_Job.write("%d\n" %Num_Laminas)
    Arquivo_Job.write("1") # Número de passos
    
    # Arquivo Coord
    for I in range(NN):
        Arquivo_Coord.write("%5d %16.6e %16.6e %16.6e\n" 
                           %(Coordenadas[I][0],Coordenadas[I][1],Coordenadas[I][2],Coordenadas[I][3]))
    
    # Arquivo Incidencia
    for I in range(NE):
        Arquivo_Incidencia.write("%5d %5d %5d %5d %5d\n" 
                           %(I+1,Incidencia[I][0],Incidencia[I][1],Incidencia[I][2],Incidencia[I][3]))
    
    # Arquivo Monitorados
    for I in range(Num_Nos_Monitorados):
        Arquivo_Monitorados.write("%d\n" %Nos_Monitorados[I])
    
    # Arquivo Desl
    for I in range(NN):
        Arquivo_Desl.write("%5d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                     %(I+1,Desl[6*I],Desl[6*I+1],Desl[6*I+2],Desl[6*I+3],Desl[6*I+4],Desl[6*I+5]))
    
    # Arquivo Esf
    for I in range(NN):
        Arquivo_Esf.write("%5d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                    %(ESFORCOS[I][0],ESFORCOS[I][1],ESFORCOS[I][2],ESFORCOS[I][3],ESFORCOS[I][4],ESFORCOS[I][5],ESFORCOS[I][6]))
    
    # Arquivo Deform
    for I in range(Num_Laminas):
        for J in range(NN):
            Arquivo_Deform.write("%5d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                           %(Deformacao[I][J][0],Deformacao[I][J][1],Deformacao[I][J][2],Deformacao[I][J][3],Deformacao[I][J][4],Deformacao[I][J][5],Deformacao[I][J][6]))
                   
    # Arquivo Tens
    for I in range(Num_Laminas):
        for J in range(NN):
            Arquivo_Tens.write("%5d %16.6e %16.6e %16.6e %16.6e %16.6e %16.6e\n"
                           %(Tensao[I][J][0],Tensao[I][J][1],Tensao[I][J][2],Tensao[I][J][3],Tensao[I][J][4],Tensao[I][J][5],Tensao[I][J][6]))
    
    # Fechar os arquivos
    Arquivo_Job.close()
    Arquivo_Coord.close()
    Arquivo_Incidencia.close()
    Arquivo_Monitorados.close()
    Arquivo_Desl.close()
    Arquivo_Esf.close()
    Arquivo_Deform.close()
    Arquivo_Tens.close()
    
def fun_contar_tempo(Hora,Nome_Funcao):
    global HoraAux
    if Nome_Funcao == "total":
        Minutos = float((Hora-HoraInicio)/60)
        MinutosS = str("%6.6f") %(Minutos)
        MinutosS = MinutosS[:MinutosS.find(".")]
        Segundos = float((Hora-HoraInicio)%60)
        print ("Tempo %s: %d minutos %.2f segundos" %(Nome_Funcao,int(MinutosS),Segundos))
    else:    
        Minutos = float((Hora-HoraAux)/60)
        MinutosS = str("%6.6f") %(Minutos)
        MinutosS = MinutosS[:MinutosS.find(".")]
        Segundos = float((Hora-HoraAux)%60)
        HoraAux = Hora
        print ("Tempo funcao %s: %d minutos %.2f segundos" %(Nome_Funcao,int(MinutosS),Segundos))
            

fun_main()
